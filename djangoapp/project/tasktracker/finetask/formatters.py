from .library.logging import log_call


from argparse import Namespace


class Formatter:
    def __init__(self, storage):
        self.storage = storage

    @log_call
    def format_task(self, task):
        """Return string representation of task. If task is string message, return it."""

        properties = task.to_str_list()

        owner = self.storage.extract_user(task.owner_id)
        if owner is not None:
            properties.append('Owner: {user}'.format(user=owner))

        planner = self.storage.extract_planner(task.planner_id)
        if planner is not None:
            properties.append('Planner: {plr}'.format(plr=planner))

        parent = self.storage.extract_task(task.parent_id)
        if parent is not None:
            properties.append('Parent: {pnt}'.format(pnt=parent))

        dependency = self.storage.extract_dependency(task.id)
        if dependency is not None:
            properties.append('Dependency: {dep}'.format(dep=self.format_dependency(dependency)))

        access_list = self.storage.find_all_granted_permissions(task.id)
        if access_list is not None:
            properties += list(map(self.format_permission, access_list))

        return properties

    @log_call
    def format_task_tree(self, task, tabs=''):
        """
        Return task tree in the following format:

        * task1
            * subtask1
                * sub-subtask11
                * sub-subtask12
            * subtask2
                * sub-subtask21
                * sub-subtask22

        task -- models.Task

        """

        response = []
        response.append(Namespace(align=tabs+"* ", task=str(task), id=task.id))

        for task in self.storage.find_subtasks(task.id):
            response += self.format_task_tree(task, tabs=tabs + "\t\t")

        return response

    @log_call
    def format_dependency(self, dependency):
        main_task = self.storage.extract_task(dependency.taskB_id)
        return 'depends on [ {main_task} ] with {dep}'.format(main_task=main_task, dep=dependency)

    @log_call
    def format_permission(self, permission):
        user = self.storage.extract_user(permission.guest_id)
        return '{permission} for {user}'.format(permission=permission, user=user)