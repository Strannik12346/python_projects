from datetime import datetime, date, time
from django.contrib.auth import login, authenticate

from .library.interaction import (
    SHIFT_TYPE_HOUR,
    SHIFT_TYPE_HOURS,
    SHIFT_TYPE_DAY,
    SHIFT_TYPE_DAYS,
    SHIFT_TYPE_WEEK,
    SHIFT_TYPE_WEEKS
)

from .library.models import (
    PRIORITY_HIGH,
    PRIORITY_MEDIUM,
    PRIORITY_LOW
)

from django.contrib.auth.forms import UserCreationForm
from django.forms.widgets import SelectDateWidget, Select
from django.forms.forms import Form
from django.forms import (
    IntegerField,
    CharField,
    DateField,
    ChoiceField,
    EmailField
)

from .library import interaction
from .library import models


def double_digit(integer):
    return '0' + str(integer) if integer < 10 else str(integer)


class AddTaskForm(Form):
    name = CharField(
        label='Name'
    )
    content = CharField(
        label='Content'
    )
    deadline_date = DateField(
        widget=SelectDateWidget,
        initial=date.today(),
        label='Deadline'
    )
    deadline_hours = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(24)]),
        initial=12
    )
    deadline_minutes = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(60)]),
        initial=0
    )
    priority = ChoiceField(
        choices=(
            (PRIORITY_LOW, PRIORITY_LOW),
            (PRIORITY_MEDIUM, PRIORITY_MEDIUM),
            (PRIORITY_HIGH, PRIORITY_HIGH)
        ),
        initial=PRIORITY_MEDIUM,
        label='Priority'
    )

    @staticmethod
    def process(controller, form, user):
        data = form.cleaned_data
        name = data['name']
        content = data['content']
        str_priority = data['priority']
        priority = models.Task.get_int_priority(str_priority)

        deadline_hours = data['deadline_hours']
        deadline_minutes = data['deadline_minutes']
        deadline_time = time(
            hour=deadline_hours,
            minute=deadline_minutes,
            second=0,
            microsecond=0
        )

        deadline_date = data['deadline_date']

        time_created = datetime.now().strftime(interaction.DATETIME_FORMAT)
        time_deadline = datetime.combine(deadline_date, deadline_time).strftime(interaction.DATETIME_FORMAT)

        task = models.Task(name, content, interaction.STATUS_CREATED,
                           time_created, time_deadline, priority, user.id)
        controller.add_task(task)


class AddPlannerForm(AddTaskForm):
    shift = IntegerField(
        widget=Select(choices=[(val, val) for val in range(100)]),
        initial=1,
        label='Shift'
    )
    shift_type = ChoiceField(
        choices=(
            (SHIFT_TYPE_HOUR, SHIFT_TYPE_HOUR),
            (SHIFT_TYPE_HOURS, SHIFT_TYPE_HOURS),
            (SHIFT_TYPE_DAY, SHIFT_TYPE_DAY),
            (SHIFT_TYPE_DAYS, SHIFT_TYPE_DAYS),
            (SHIFT_TYPE_WEEK, SHIFT_TYPE_WEEK),
            (SHIFT_TYPE_WEEKS, SHIFT_TYPE_WEEKS),
        ),
        initial=SHIFT_TYPE_WEEK
    )

    @staticmethod
    def process(controller, form, user):
        data = form.cleaned_data
        name = data['name']
        content = data['content']
        str_priority = data['priority']
        priority = models.Task.get_int_priority(str_priority)

        deadline_hours = data['deadline_hours']
        deadline_minutes = data['deadline_minutes']
        deadline_time = time(
            hour=deadline_hours,
            minute=deadline_minutes,
            second=0,
            microsecond=0
        )

        deadline_date = data['deadline_date']
        time_deadline = datetime.combine(deadline_date, deadline_time).strftime(interaction.DATETIME_FORMAT)

        shift = data['shift']
        shift_type = data['shift_type']

        planner = models.Planner(name, content, time_deadline, priority, shift, shift_type, user.id)
        controller.add_planner(planner)


class AddReminderForm(Form):
    text = CharField(
        label='Text'
    )
    invoke_date = DateField(
        widget=SelectDateWidget,
        initial=date.today(),
        label='Invoke time'
    )
    invoke_time_hours = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(24)]),
        initial=12
    )
    invoke_time_minutes = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(60)]),
        initial=0
    )

    @staticmethod
    def process(controller, form, user):
        data = form.cleaned_data
        text = data['text']

        invoke_time_hours = data['invoke_time_hours']
        invoke_time_minutes = data['invoke_time_minutes']
        invoke_time = time(
            hour=invoke_time_hours,
            minute=invoke_time_minutes,
            second=0,
            microsecond=0
        )

        invoke_date = data['invoke_date']
        combined_invoke_time = datetime.combine(invoke_date, invoke_time).strftime(interaction.DATETIME_FORMAT)

        reminder = models.Reminder(text, combined_invoke_time, user.id)
        controller.add_reminder(reminder)


class AddRegularReminderForm(AddReminderForm):
    shift = IntegerField(
        widget=Select(choices=[(val, val) for val in range(100)]),
        initial=1,
        label='Shift'
    )
    shift_type = ChoiceField(
        choices=(
            (SHIFT_TYPE_HOUR, SHIFT_TYPE_HOUR),
            (SHIFT_TYPE_HOURS, SHIFT_TYPE_HOURS),
            (SHIFT_TYPE_DAY, SHIFT_TYPE_DAY),
            (SHIFT_TYPE_DAYS, SHIFT_TYPE_DAYS),
            (SHIFT_TYPE_WEEK, SHIFT_TYPE_WEEK),
            (SHIFT_TYPE_WEEKS, SHIFT_TYPE_WEEKS),
        ),
        initial=SHIFT_TYPE_WEEK
    )

    @staticmethod
    def process(controller, form, user):
        data = form.cleaned_data
        text = data['text']

        invoke_time_hours = data['invoke_time_hours']
        invoke_time_minutes = data['invoke_time_minutes']
        invoke_time = time(
            hour=invoke_time_hours,
            minute=invoke_time_minutes,
            second=0,
            microsecond=0
        )

        invoke_date = data['invoke_date']
        combined_invoke_time = datetime.combine(invoke_date, invoke_time).strftime(interaction.DATETIME_FORMAT)

        shift = data['shift']
        shift_type = data['shift_type']

        reminder = models.Reminder(text, combined_invoke_time, user.id, shift, shift_type)
        controller.add_reminder(reminder)


class EditTaskForm(Form):
    name = CharField(
        label='Name'
    )
    content = CharField(
        label='Content'
    )
    priority = ChoiceField(
        choices=(
            (PRIORITY_LOW, PRIORITY_LOW),
            (PRIORITY_MEDIUM, PRIORITY_MEDIUM),
            (PRIORITY_HIGH, PRIORITY_HIGH)
        ),
        initial=PRIORITY_MEDIUM,
        label='Priority'
    )

    @staticmethod
    def set_initials(controller, form, user, task_id):
        task = controller.get_task_by_id(user.id, task_id)
        form.fields['name'].initial = task.name
        form.fields['content'].initial = task.content
        form.fields['priority'].initial = models.Task.get_string_priority(task.priority)

    @staticmethod
    def process(controller, form, user, task_id):
        data = form.cleaned_data
        name = data['name']
        content = data['content']
        str_priority = data['priority']
        priority = models.Task.get_int_priority(str_priority)

        controller.change_task(user.id, task_id, name=name, content=content, priority=priority)


class EditPlannerForm(EditTaskForm):
    @staticmethod
    def set_initials(controller, form, user, planner_id):
        planner = controller.get_planner_by_id(user.id, planner_id)
        form.fields['name'].initial = planner.task_name
        form.fields['content'].initial = planner.task_content
        form.fields['priority'].initial = models.Task.get_string_priority(planner.task_priority)

    @staticmethod
    def process(controller, form, user, planner_id):
        data = form.cleaned_data
        name = data['name']
        content = data['content']
        str_priority = data['priority']
        priority = models.Task.get_int_priority(str_priority)

        controller.change_planner(user.id, planner_id, name=name, content=content, priority=priority)


class EditReminderForm(Form):
    text = CharField(
        label='Text'
    )
    invoke_date = DateField(
        widget=SelectDateWidget,
        initial=date.today(),
        label='Invoke time'
    )
    invoke_time_hours = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(24)]),
        initial=12
    )
    invoke_time_minutes = IntegerField(
        widget=Select(choices=[(val, double_digit(val)) for val in range(60)]),
        initial=0
    )

    @staticmethod
    def process(controller, form, user, reminder_id):
        data = form.cleaned_data
        text = data['text']

        invoke_time_hours = data['invoke_time_hours']
        invoke_time_minutes = data['invoke_time_minutes']
        invoke_time = time(
            hour=invoke_time_hours,
            minute=invoke_time_minutes,
            second=0,
            microsecond=0
        )

        invoke_date = data['invoke_date']
        combined_invoke_time = datetime.combine(invoke_date, invoke_time).strftime(interaction.DATETIME_FORMAT)

        controller.change_reminder(user.id, reminder_id, text=text, time=combined_invoke_time)

    @staticmethod
    def set_initials(controller, form, user, reminder_id):
        reminder = controller.get_reminder_by_id(user.id, reminder_id)
        form.fields['text'].initial = reminder.text

        invoke_time = datetime.strptime(reminder.invoke_time, interaction.DATETIME_FORMAT)
        form.fields['invoke_date'].initial = invoke_time.date()
        form.fields['invoke_time_hours'].initial = invoke_time.hour
        form.fields['invoke_time_minutes'].initial = invoke_time.minute


class UserCreationFormWithEmail(UserCreationForm):
    email = EmailField(initial='sample@gmail.com')

    @staticmethod
    def process(controller, form, request):
        form.save()
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        email = form.cleaned_data.get('email')
        user = authenticate(username=username, password=raw_password)
        login(request, user)  # Django authentification

        user = models.User(username, email, 0)
        controller.add_user(user)
