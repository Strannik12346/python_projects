from django.conf.urls import url

from finetask import views


urlpatterns = [
    url(r'^(?P<reminder_id>[0-9]*)/$', views.show_reminder),
    url(r'^(?P<reminder_id>[0-9]*)/edit/$', views.edit_reminder),
    url(r'^add/$', views.add_reminder),
    url(r'^add_regular/$', views.add_regular_reminder),
    url(r'^$', views.reminders)
]