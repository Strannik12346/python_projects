from django.conf.urls import url

from finetask import views


urlpatterns = [
    url(r'^(?P<planner_id>[0-9]*)/$', views.show_planner),
    url(r'^(?P<planner_id>[0-9]*)/edit/$', views.edit_planner),
    url(r'^add/$', views.add_planner),
    url(r'^$', views.planners)
]