from django.conf.urls import url

from finetask import views


urlpatterns = [
    url(r'^(?P<task_id>[0-9]*)/$', views.show_task),
    url(r'^(?P<task_id>[0-9]*)/edit/$', views.edit_task),
    url(r'^(?P<task_id>[0-9]*)/tree/$', views.show_task_tree),
    url(r'^add/$', views.add_task),
    url(r'^groups/$', views.show_task_groups),
    url(r'^groups/(?P<group_name>[a-zA-Z]*)/$', views.show_tasks_in_group),
    url(r'^attach/(?P<root_id>[0-9]*)/$', views.add_subtask),
    url(r'^dependency/(?P<task_id>[0-9]*)/$', views.edit_dependency),
    url(r'^move/(?P<task_id>[0-9]*)/$', views.move_to_group),
    url(r'^permissions/(?P<task_id>[0-9]*)/$', views.edit_permissions),
    url(r'^foreign/$', views.foreign_tasks),
    url(r'^$', views.tasks),
]