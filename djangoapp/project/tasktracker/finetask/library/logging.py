import logging
from functools import wraps


def log_call(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        logger = get_logger()
        logger.debug('In function {funcname}'.format(funcname=fun.__name__))
        return fun(*args, **kwargs)

    return wrapper


def get_logger():
    return logging.getLogger('finetask')