from datetime import datetime
from datetime import timedelta
from sqlalchemy.exc import IntegrityError

from finetask.library.storage import Storage
from finetask.library.storage import PERMISSION_OWNER
from finetask.library.models import Dependency
from finetask.library.logging import log_call
import finetask.library.exceptions as exceptions


DATETIME_FORMAT = '%d.%m.%Y %H:%M'

ROOT_USERNAME = 'root'
ROOT_EMAIL = 'sample@gmail.com'

TEST_USERNAME = 'test'
TEST_EMAIL = 'test@gmail.com'

STATUS_CREATED = 'Created'
STATUS_IN_PROGRESS = 'In Progress'
STATUS_DONE = 'Done'
STATUS_FAILED = 'Failed'

BEHAVIOUR_NONE = 'No Dependency'
BEHAVIOUR_DIRECT = 'Direct'
BEHAVIOUR_INVERSE = 'Inverse'
BEHAVIOUR_PARALLEL = 'Parallel'

GROUP_ALL = 'All'
GROUP_CURRENT = 'Current'
GROUP_ENDED = 'Ended'
GROUP_DONE = 'Done'
GROUP_FAILED = 'Failed'
GROUP_ARCHIEVE = 'Archieve'

SHIFT_TYPE_HOUR = 'hour'
SHIFT_TYPE_HOURS = 'hours'
SHIFT_TYPE_DAY = 'day'
SHIFT_TYPE_DAYS = 'days'
SHIFT_TYPE_WEEK = 'week'
SHIFT_TYPE_WEEKS = 'weeks'

PERMISSION_READ = 'can_read'
PERMISSION_CONFIRM = 'can_confirm'
PERMISSION_EDIT = 'can_edit'


class Interaction:
    def __init__(self, filename):
        """
        Specify file that will be treated as SQLite database.

        If tables don't exist, create them. If user 'root' doesn't exist,
        insert it with sample@gmail.com and rate 10. Do the same with
        user 'test'.

        filename -- str

        """

        self.datetime_format = DATETIME_FORMAT
        self.storage = Storage(filename)

        if self.get_user_by_name(ROOT_USERNAME) is None:
            self.storage.insert_user(ROOT_USERNAME, ROOT_EMAIL, 0)  # Sample user is 'root'

        if self.get_user_by_name(TEST_USERNAME) is None:
            self.storage.insert_user(TEST_USERNAME, TEST_EMAIL, 0)  # And another user 'test'

    # REGION GET

    @log_call
    def get_user_by_name(self, username):
        """
        Return user with name=username from the database or None.

        username -- str

        """

        user = self.storage.find_user(username)
        return user if user is not None else None

    @log_call
    def get_user_by_id(self, user_id):
        """
        Return user with id=user_id from the database or raise NoUserError.

        user_id -- int > 0

        """

        user = self.storage.extract_user(user_id)

        if user is None:
            raise exceptions.NoUserError()

        return user

    @log_call
    def get_task_by_id(self, current_user_id, task_id):
        """
        Return task with id=task_id from the database or raise NoTaskError.

        If user doesn't have permissions, raise PermissionDeniedError.

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0

        """

        if not self.has_read_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        task = self.storage.extract_task(task_id)

        if task is None:
            raise exceptions.NoTaskError()

        return task

    @log_call
    def get_planner_by_id(self, current_user_id, planner_id):
        """
        Return planner with id=planner_id from the database or raise NoPlannerError.

        If user doesn't have permissions, raise PermissionDeniedError.

        current_user_id -- int > 0 -- identified user
        planner_id -- int > 0

        """

        planner = self.storage.extract_planner(planner_id)

        if planner is None:
            raise exceptions.NoPlannerError()

        if current_user_id != planner.owner_id:
            raise exceptions.PermissionDeniedError()

        return planner

    @log_call
    def get_reminder_by_id(self, current_user_id, reminder_id):
        """
        Return reminder with id=reminder_id from the database or raise NoReminderError.

        If user doesn't have permissions, raise PermissionDeniedError.

        current_user_id -- int > 0 -- identified user
        reminder_id -- int > 0

        """

        reminder = self.storage.extract_reminder(reminder_id)

        if reminder is None:
            raise exceptions.NoReminderError()

        if current_user_id != reminder.owner_id:
            raise exceptions.PermissionDeniedError()

        return reminder

    @log_call
    def get_dependency_by_task_id(self, current_user_id, task_id):
        """
        Return dependency of the task with id=task_id from the database or raise NoDependencyError.

        If user doesn't have permissions, raise PermissionDeniedError.

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0

        """

        dependency = self.storage.extract_dependency(task_id)

        if dependency is None:
            raise exceptions.NoDependencyError()

        if current_user_id != dependency.owner_id:
            raise exceptions.PermissionDeniedError()

        return dependency

    @log_call
    def get_tasks_by_group_name(self, user_id, group_name):
        """
        Return all the tasks that refer to the group.

        user_id -- int > 0
        group_name -- str

        """

        standard_groups = {
            GROUP_ALL: self.storage.find_all_tasks,
            GROUP_CURRENT: lambda user_id: self.storage.find_tasks_by_status(user_id, STATUS_CREATED) +
                                           self.storage.find_tasks_by_status(user_id, STATUS_IN_PROGRESS),
            GROUP_ENDED: lambda user_id: self.storage.find_tasks_by_status(user_id, STATUS_DONE) +
                                         self.storage.find_tasks_by_status(user_id, STATUS_FAILED),
            GROUP_DONE: lambda user_id: self.storage.find_tasks_by_status(user_id, STATUS_DONE),
            GROUP_FAILED: lambda user_id: self.storage.find_tasks_by_status(user_id, STATUS_FAILED)
        }

        seq_tasks = list()
        try:
            get_tasks_fun = standard_groups[group_name]
            seq_tasks = get_tasks_fun(user_id)
        except:
            seq_tasks = self.storage.extract_group(user_id, group_name)

        return sorted(seq_tasks, key=lambda task: task.priority, reverse=True)

    @log_call
    def get_all_users(self):
        """Return list of all the users in the database."""

        users = self.storage.find_all_users()
        return users

    @log_call
    def get_all_tasks(self, owner_id, skip_archieve=True):
        """
        Return list of tasks for this user.

        owner_id -- int > 0

        """

        tasks = sorted(self.storage.find_all_tasks(owner_id), key=lambda x: x.id)

        if skip_archieve:  # Removing archieve tasks
            archieve = sorted(self.storage.extract_group(owner_id, GROUP_ARCHIEVE), key=lambda x: x.id)

            i = 0
            while len(archieve) > 0:
                if tasks[i].id == archieve[0].id:
                    tasks.remove(tasks[i])
                    archieve.remove(archieve[0])
                elif tasks[i].id < archieve[0].id:
                    i += 1

        return sorted(tasks, key=lambda task: task.priority, reverse=True)

    @log_call
    def get_all_planners(self, owner_id):
        """
        Return list of planners for this user.

        owner_id -- int > 0

        """

        planners = self.storage.find_all_planners(owner_id)
        return planners

    @log_call
    def get_all_reminders(self, owner_id):
        """
        Return list of reminders for this user.

        owner_id -- int > 0

        """

        reminders = self.storage.find_all_reminders(owner_id)
        return reminders

    @log_call
    def get_all_groups(self, owner_id):
        """
        Return list of groups for this user.

        owner_id -- int > 0

        """

        groups = self.storage.extract_group_names(owner_id)

        if GROUP_ARCHIEVE in groups:
            groups.remove(GROUP_ARCHIEVE)  # Not to have 2 archieves

        return [GROUP_ALL, GROUP_CURRENT, GROUP_ENDED,
                GROUP_DONE, GROUP_FAILED, GROUP_ARCHIEVE] + groups

    # REGION ADDING

    @log_call
    def add_user(self, user):
        """
        Insert user. Return its id.

        If such a username exists, raise ExistingUserError.

        user -- models.User

        """

        try:
            user = self.storage.insert_user(user.nickname, user.email, user.rate)
            return user.id
        except IntegrityError:
            raise exceptions.ExistingUserError()

    @log_call
    def add_task(self, task):
        """
        Insert task into database. Return its id.

        task -- models.Task

        """

        task = self.storage.insert_task(task.name, task.content, task.status, task.time_created, task.time_deadline,
                                        task.priority, task.owner_id, task.planner_id, task.parent_id)
        return task.id

    @log_call
    def add_planner(self, planner):
        """
        Insert planner into database. Return its id.

        planner -- models.Planner

        """

        planner = self.storage.insert_planner(planner.task_name, planner.task_content, planner.task_deadline,
                                              planner.task_priority, planner.shift,
                                              planner.shift_type, planner.owner_id)
        self._plan_first_task(planner.id)
        return planner.id

    @log_call
    def add_reminder(self, reminder):
        """
        Insert reminder into database. Return its id.

        reminder -- models.Reminder

        """

        reminder = self.storage.insert_reminder(reminder.text, reminder.invoke_time,
                                                reminder.shift, reminder.shift_type, reminder.owner_id)
        return reminder.id

    @log_call
    def add_dependency(self, current_user_id, dependency):
        """
        Insert dependency into database. Return its id.

        If dependency exists, raise ExistingDependencyError.
        If user is not owner, raise PermissionDeniedError.

        dependency -- models.Dependency

        """

        if self.storage.extract_dependency(dependency.taskA_id) is not None:
            raise exceptions.ExistingDependencyError()

        if not self.has_delete_access(current_user_id, dependency.taskA_id):
            raise exceptions.PermissionDeniedError()

        dependency = self.storage.insert_dependency(dependency.taskA_id, dependency.taskB_id,
                                                    dependency.behaviour, dependency.owner_id)
        return dependency.id

    # REGION CHANGING

    @log_call
    def change_user(self, user_id, name=None, email=None, rate=None):
        """
        Change user properties. Return True.

        If name is not unique, raise ExistingUserError
        If user does not exist, raise NoUserError

        user_id -- int > 0
        name -- unique string
        email -- string
        rate -- int

        """

        if self.storage.find_user(name) is not None:
            raise exceptions.ExistingUserError()

        if self.storage.extract_user(user_id) is None:
            raise exceptions.NoUserError()

        self.storage.update_user(user_id, new_name=name, new_email=email, new_rate=rate)

        return True

    def change_task(self, current_user_id, task_id, name=None, content=None, status=None, priority=None):
        """
        Change task properties. Return True.

        If task doesn't exist, raise NoTaskError.
        If user doesn't have permissions, raise PermissionDeniedError.
        If status cannot be changed by logic, raise StatusNotAllowedError.

        current_user_id -- int > 0 -- identified user
        task_id -- int
        name -- str
        content -- str
        status -- 'Created', 'In Progress', 'Done' or 'Failed'
        priority -- int

        """

        task = self.storage.extract_task(task_id)

        if task is None:
            raise exceptions.NoTaskError()

        requires_confirm = status is not None

        if requires_confirm and not self.has_confirm_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        requires_edit = any(attr is not None for attr in [name, content, priority])

        if requires_edit and not self.has_edit_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        if status is not None and not self._is_allowed_to_set(task_id, status):
            raise exceptions.StatusNotAllowedError(task_id, status)

        self.storage.update_task(task_id, new_name=name, new_content=content, new_status=status, new_priority=priority)

        if (status == STATUS_DONE or status == STATUS_FAILED) and task.planner_id is not None:
            self._plan_new_task(task.planner_id)

        if status == STATUS_DONE:  # Increasing user rating
            user = self.get_user_by_id(task.owner_id)
            self.change_user(user.id, rate=user.rate + 10)

        return True

    @log_call
    def detach_task(self, current_user_id, task_id):
        """
        Remove the task and its subtasks from the task tree. Return True.

        If task doesn't exist, raise NoTaskError.
        If user doesn't have permissions, raise PermissionDeniedError.

        To detach task user must have at least
        'can_edit' permissions.

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0

        """

        if self.storage.extract_task(task_id) is None:
            raise exceptions.NoTaskError()

        if not self.has_edit_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        self.storage.update_task(task_id, new_parent_id=None)
        return True

    @log_call
    def _flatten_hierarchy(self, root_id):
        root = self.storage.extract_task(root_id)
        subtasks = self.storage.find_subtasks(root_id)
        sub_subtasks = list()
        for el in subtasks:
            sub_subtasks += self._flatten_hierarchy(el.id)
        return [root] + subtasks + sub_subtasks

    @log_call
    def _hierarchy_contains(self, task_id, root_id):
        for el in self._flatten_hierarchy(task_id):
            if el.id == root_id:
                return True
        return False

    @log_call
    def attach_task(self, current_user_id, task_id, root_id):
        """
        Make the task a subtask of some other task. Return True.

        If task doesn't exist, raise NoTaskError.
        If root doesn't exist, raise InvalidAttachError.
        If user doesn't have permissions, raise PermissionDeniedError.
        If cycle was detected, raise TaskTreeCycleError.

        Check the cycle and permissions for the user.
        To attach task user must have permission with
        level at least 'can_edit'

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0 (attached task)
        root_id -- int > 0

        """

        if self.storage.extract_task(task_id) is None:
            raise exceptions.NoTaskError()

        if self.storage.extract_task(root_id) is None:
            raise exceptions.InvalidAttachError(root_id)

        if not self.has_edit_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        if self._hierarchy_contains(task_id, root_id):
            raise exceptions.TaskTreeCycleError

        self.storage.update_task(task_id, new_parent_id=root_id)
        return True

    @log_call
    def change_planner(self, current_user_id, planner_id, name=None, content=None, priority=None):
        """
        Change planner properties. Return True.

        If planner doesn't exist, raise NoPlannerError.
        If user doesn't have permissions, raise PermissionDeniedError.

        It won't affect the already created tasks.
        Only owner can change his planners.

        current_user_id -- int > 0 -- identified user
        planner_id -- int > 0
        name -- str
        content -- str
        priority -- int

        """

        planner = self.storage.extract_planner(planner_id)

        if planner is None:
            raise exceptions.NoPlannerError()

        if current_user_id != planner.owner_id:
            raise exceptions.PermissionDeniedError()

        self.storage.update_planner(planner_id, new_name=name, new_content=content, new_priority=priority)

        return True

    @log_call
    def change_reminder(self, current_user_id, reminder_id, text=None, time=None):
        """
        Change the reminder properties. Return True.

        If reminder doesn't exist, raise NoReminderError.
        If user doesn't have permissions, raise PermissionDeniedError.

        Only owner can change his reminders.

        current_user_id -- int > 0 -- identified user
        reminder_id -- int > 0
        text -- str
        time -- str in the format '23.10.2010 13:10'

        """

        reminder = self.storage.extract_reminder(reminder_id)

        if reminder is None:
            raise exceptions.NoReminderError()

        if current_user_id != reminder.owner_id:
            raise exceptions.PermissionDeniedError()

        self.storage.update_reminder(reminder_id, new_text=text, new_invoke_time=time)

        return True

    @log_call
    def remove_user(self, current_user_id, user_id):  # Also removes everything owned by this user
        """
        Completely remove user with id=user_id. Return True.

        If user doesn't exist, raise NoUserError.
        If user doesn't have permissions, raise PermissionDeniedError.

        Also remove everything owned by him.
        Only user can remove himself.

        current_user_id -- int > 0 -- identified user
        user_id -- int > 0

        """

        user = self.storage.extract_user(user_id)
        if user is None:
            raise exceptions.NoUserError()

        if current_user_id != user_id:
            raise exceptions.PermissionDeniedError()

        self.storage.remove_user(user_id)
        return True

    @log_call
    def remove_task(self, current_user_id, task_id):
        """
        Completely remove task with id=task_id. Return True.

        Also removes dependency for this task.
        Removes the planner for this task if the task is
        not ended yet (it's the last task for this planner).

        If task doesn't exist, raise NoTaskError.
        If user doesn't have permissions, raise PermissionDeniedError.
        If task has subtasks, raise InvalidTaskDeleteError.

        Only owner can remove his tasks.

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0

        """

        task = self.storage.extract_task(task_id)

        if task is None:
            raise exceptions.NoTaskError()

        if not self.has_delete_access(current_user_id, task_id):
            raise exceptions.PermissionDeniedError()

        if self._has_subtasks(task_id):
            raise exceptions.InvalidTaskDeleteError(task_id)

        self.storage.remove_task(task_id)
        self.storage.remove_dependency(task_id)

        if task.status not in [STATUS_DONE, STATUS_FAILED] and task.planner_id is not None:
            self.storage.remove_planner(task.planner_id)

        return True

    @log_call
    def remove_planner(self, current_user_id, planner_id):
        """
        Completely remove planner with id=planner_id. Return True.

        If planner doesn't exist, raise NoPlannerError.
        If user doesn't have permissions, raise PermissionDeniedError.

        Only owner can remove his planners.

        current_user_id -- int > 0 -- identified user
        planner_id -- int > 0

        """

        planner = self.storage.extract_planner(planner_id)

        if planner is None:
            raise exceptions.NoPlannerError()

        if current_user_id != planner.owner_id:
            raise exceptions.PermissionDeniedError()

        self.storage.remove_planner(planner_id)
        return True

    @log_call
    def remove_reminder(self, current_user_id, reminder_id):
        """
        Completely remove reminder with id=reminder_id. Return True.

        If reminder doesn't exist, raise NoReminderError.
        If user doesn't have permissions, raise PermissionDeniedError.

        Only owner can remove his reminders

        current_user_id -- int > 0 -- identified user
        reminder_id -- int > 0

        """

        reminder = self.storage.extract_reminder(reminder_id)

        if reminder is None:
            raise exceptions.NoReminderError()

        if current_user_id != reminder.owner_id:
            raise exceptions.PermissionDeniedError()

        self.storage.remove_reminder(reminder_id)
        return True

    @log_call
    def remove_dependency(self, current_user_id, task_id):
        """
        Remove dependency for this task. Return True.

        If dependency doesn't exist, raise NoDependencyError.
        If user doesn't have permissions, raise PermissionDeniedError.

        Only owner can remove task dependency.

        current_user_id -- int > 0 -- identified user
        task_id -- int > 0

        """

        dependency = self.storage.extract_dependency(task_id)

        if dependency is None:
            raise exceptions.NoDependencyError()

        if not self.has_delete_access(current_user_id, dependency.taskA_id):
            raise exceptions.PermissionDeniedError()

        self.storage.remove_dependency(task_id)
        return True

    # REGION GROUPS

    @log_call
    def move_task_to_group(self, current_user_id, task_id, group_name):
        """
        Add task to custom group. Return True.

        If group is standard, raise InvalidGroupMoveError.
        If task doesn't exist, raise NoTaskError.
        If user is not owner of the task, raise PermissionDeniedError.

        Due to special logic users can't add to virtual
        groups 'All', 'Current', 'Ended', 'Done', 'Failed' and 'Archieve'.

        task_id -- int > 0
        group_name -- str

        """

        if group_name in [GROUP_ALL, GROUP_CURRENT, GROUP_ENDED, GROUP_DONE, GROUP_FAILED]:
            raise exceptions.InvalidGroupMoveError

        task = self.storage.extract_task(task_id)

        if task is None:
            raise exceptions.NoTaskError()

        if task.owner_id != current_user_id:
            raise exceptions.PermissionDeniedError()

        self.storage.add_to_group(group_name, task.id, current_user_id)
        return True

    @log_call
    def remove_task_from_group(self, current_user_id, task_id, group_name):
        """
        Remove task from group. Return True.

        If task doesn't exist, raise NoTaskError.
        If user is not owner of the task, raise PermissionDeniedError.

        Due to special logic we cannot delete from virtual
        groups 'All', 'Current', 'Ended', 'Done', 'Failed' and 'Archieve'.

        task_id -- int > 0
        group_name -- str

        """

        task = self.storage.extract_task(task_id)

        if task is None:
            raise exceptions.NoTaskError()

        if task.owner_id != current_user_id:
            raise exceptions.PermissionDeniedError()

        self.storage.remove_from_group(task.owner_id, group_name, task.id)
        return True

    # REGION TASKS_AND_REMINDERS

    @log_call
    def _is_allowed_to_set(self, task_id, new_status):
        """
        Return logic permission to change task status (True / False).

        Return AND of three statements:
        task is not ended yet,
        task is not dependant OR dependency allows,
        subtasks are completed OR don't exist.

        Service function. Do not import it
        if you are using the class outside.

        task_id -- int > 0
        new_status -- 'Created', 'In Progress', 'Done' or 'Failed'

        """

        task = self.storage.extract_task(task_id)
        statement1 = task.status != STATUS_DONE and task.status != STATUS_FAILED  # Task is not ended yet

        dep = self.storage.extract_dependency(task_id)
        task = self.storage.extract_task(dep.taskB_id) if dep is not None else None
        statement2 = (task is None or
                      Dependency.allows_to_set(task.status, dep.behaviour, new_status))  # Dependency allows

        subtasks = self.storage.find_subtasks(task_id)
        statement3 = True

        if new_status == STATUS_DONE and subtasks is not None:
            for task in subtasks:
                statement3 = statement3 and task.status == STATUS_DONE  # Subtasks are completed or don't exist

        return statement1 and statement2 and statement3

    @log_call
    def _has_subtasks(self, task_id):
        """
        Return bool of the statement:

        task with task_id is not a leaf in task tree.
        Service function. Do not import it if
        you are using the class outside.

        task_id -- int > 0

        """

        seq = self.storage.find_subtasks(task_id)
        return seq is not None and len(seq) > 0

    def _time_to_remind(self, reminder):
        """
        Return if it's time to show reminder to user (True / False).

        Check if the reminder invoke time is in (now - eps, now + eps).
        The value eps is counted using reminder shift and shift type, but is
        a much more small value.

        Service function. Do not import it if
        you are using the class outside.

        reminder -- models.Reminder

        """

        time = datetime.strptime(reminder.invoke_time, self.datetime_format)
        now = datetime.now()
        interval = timedelta(minutes=30)

        if reminder.shift_type == SHIFT_TYPE_WEEK or reminder.shift_type == SHIFT_TYPE_WEEKS:
            interval = self._make_timedelta(reminder.shift, SHIFT_TYPE_DAYS)
        elif reminder.shift_type == SHIFT_TYPE_DAY or reminder.shift_type == SHIFT_TYPE_DAYS:
            interval = self._make_timedelta(reminder.shift * 2, SHIFT_TYPE_HOURS)
        elif reminder.shift_type == SHIFT_TYPE_HOUR or reminder.shift_type == SHIFT_TYPE_HOURS:
            interval = self._make_timedelta(reminder.shift / 4, SHIFT_TYPE_HOURS)

        return now - interval < time < now + interval

    @log_call
    def try_to_remind(self, user_id):
        """
        Check all user reminders and return ones

        with invoke time between now and (now + 1h).
        Those reminders which are in the past are
        shifted to future.

        user_id -- int > 0

        """

        reminders = self.storage.find_all_reminders(user_id)
        current_reminders = list()

        if reminders is not None:
            for rem in reminders:
                if self._time_to_remind(rem):
                    current_reminders.append(rem)

        return current_reminders

    # REGION ACCESS

    @log_call
    def add_permission(self, current_user_id, permission):
        """
        Add permission. If it exists, replace it. Return True.

        If task doesn't exist, raise NoTaskError.
        If user doesn't have permissions, raise PermissionDeniedError.
        If user == guest, raise NotNecessaryPermissionError.

        current_user_id -- int > 0 -- identified user
        permission -- models.Permission

        """

        task = self.storage.extract_task(permission.task_id)
        if task is None:
            raise exceptions.NoTaskError()

        if current_user_id != task.owner_id:
            raise exceptions.PermissionDeniedError()

        if permission.owner_id == permission.guest_id:
            raise exceptions.NotNecessaryPermissionError()

        self.storage.insert_permission(permission.owner_id, permission.guest_id,
                                       permission.task_id, permission.level)

        return True

    @log_call
    def remove_permission(self, current_user_id, guest_id, task_id):
        """
        Remove permission for this user on this task. Return True.

        If permission doesn't exist, raise NoPermissionError.
        If user doesn't have permissions, raise PermissionDeniedError.

        current_user_id -- int > 0 -- identified user
        guest_id -- int > 0
        task_id -- int > 0

        """

        permission = self.storage.extract_permission(guest_id, task_id)

        if permission is None:
            raise exceptions.NoPermissionError()

        if current_user_id != permission.owner_id:
            raise exceptions.PermissionDeniedError()

        self.storage.remove_permission(guest_id, task_id)
        return True

    @log_call
    def get_all_available_tasks(self, current_user_id):
        """
        Return list of other users' tasks available to our user.

        If user doesn't exist, raise NoUserError

        current_user_id -- int > 0 -- identified user

        """

        if self.storage.extract_user(current_user_id) is None:
            raise exceptions.NoUserError()

        return self.storage.extract_foreign_tasks(current_user_id)

    @log_call
    def has_read_access(self, user_id, task_id):
        """
        Return if user has permission to read the task.

        To read the task user must either be task owner or
        have any permission for this task.

        user_id -- int > 0
        task_id -- int > 0

        """

        access = self.storage.check_permissions(user_id, task_id)
        return access in [PERMISSION_READ, PERMISSION_CONFIRM,
                          PERMISSION_EDIT, PERMISSION_OWNER, None]

    @log_call
    def has_confirm_access(self, user_id, task_id):
        """
        Return if user has permission to change task status.

        To confirm the task user must either be task owner or
        have permission 'can_confirm', or 'can_edit' for this task.

        user_id -- int > 0
        task_id -- int > 0

        """

        access = self.storage.check_permissions(user_id, task_id)
        return access in [PERMISSION_CONFIRM, PERMISSION_EDIT, PERMISSION_OWNER, None]

    @log_call
    def has_edit_access(self, user_id, task_id):
        """
        Return if user has permission to edit the task.

        To edit the task user must either be task owner or
        have permission 'can_edit' for this task.

        user_id -- int > 0
        task_id -- int > 0

        """

        access = self.storage.check_permissions(user_id, task_id)
        return access in [PERMISSION_EDIT, PERMISSION_OWNER, None]

    @log_call
    def has_delete_access(self, user_id, task_id):
        """
        Return if user has permission to delete the task.

        To delete the task user must be task owner.

        user_id -- int > 0
        task_id -- int > 0

        """

        access = self.storage.check_permissions(user_id, task_id)
        return access in [PERMISSION_OWNER, None]

    # REGION DATETIME_SHIFTING

    def _make_timedelta(self, shift, shift_type):
        """
        Return timedelta consisting of shift and its type.

        shift -- int > 0
        shift_type -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week'

        """

        if shift_type == SHIFT_TYPE_HOUR or shift_type == SHIFT_TYPE_HOURS:
            return timedelta(hours=shift)

        if shift_type == SHIFT_TYPE_DAY or shift_type == SHIFT_TYPE_DAYS:
            return timedelta(days=shift)

        if shift_type == SHIFT_TYPE_WEEK or shift_type == SHIFT_TYPE_WEEKS:
            return timedelta(weeks=shift)

    def _shift_datetime(self, str_moment, shift, shift_type):
        """
        Shift datetime to the nearest future. Return string.

        Used in shifting planners and reminders.
        Service function. Do not import it if you are using
        the class outside.

        str_moment -- str in the format '28.13.2014 23:15',
        shift -- int > 0,
        shift_type -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week'

        """

        moment = datetime.strptime(str_moment, self.datetime_format)

        delta = self._make_timedelta(shift, shift_type)
        now = datetime.now()

        while moment <= now:
            moment += delta

        return moment.strftime(self.datetime_format)

    def _shift_datetime_once(self, str_moment, shift, shift_type):
        """
        Execute moment += delta. Return string.

        Delta is defined by (shift, shift_type).
        Service function. Do not import it if
        you are using the class outside.

        str_moment -- str in the format '28.13.2014 23:15',
        shift -- int > 0,
        shift_type -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week'

        """

        moment = datetime.strptime(str_moment, self.datetime_format)
        delta = self._make_timedelta(shift, shift_type)
        moment += delta

        return moment.strftime(self.datetime_format)

    def _is_in_future(self, str_moment):
        """
        Return moment > now.

        Service function. Do not import it
        if you are using the class outside.

        str_moment -- str in the format '28.13.2014 23:15'

        """

        moment = datetime.strptime(str_moment, self.datetime_format)
        return moment >= datetime.now()

    @log_call
    def _shift_reminder(self, reminder):
        """
        Shift the reminder invoke time to the nearest
        future by its (shift, shift_type).
        Return new invoke time.

        reminder -- models.Reminder

        """

        new_invoke_time = self._shift_datetime(reminder.invoke_time, reminder.shift, reminder.shift_type)
        self.storage.update_reminder(reminder.id, new_invoke_time=new_invoke_time)

        return new_invoke_time

    @log_call
    def _shift_reminder_once(self, reminder):
        """
        Shift the reminder invoke time once
        by its (shift, shift_type).

        reminder -- models.Reminder

        """

        '''if reminder.shift is not None and reminder.shift_type is not None:'''

        new_invoke_time = self._shift_datetime_once(reminder.invoke_time, reminder.shift, reminder.shift_type)
        self.storage.update_reminder(reminder.id, new_invoke_time=new_invoke_time)

    @log_call
    def _shift_planner(self, planner):
        """
        Shift the planner invoke time by its (shift, shift_type).

        planner -- models.Planner

        """

        planner.task_deadline = (self._shift_datetime_once(planner.task_deadline, planner.shift, planner.shift_type)
                                 if self._is_in_future(planner.task_deadline)
                                 else self._shift_datetime(planner.task_deadline, planner.shift, planner.shift_type))

        self.storage.update_planner(planner.id, new_deadline=planner.task_deadline)

    @log_call
    def _shift_planner_once(self, planner):
        """
        Shift the planner invoke time once by its (shift, shift_type)

        planner -- models.Planner

        """

        planner.task_deadline = (self._shift_datetime_once(planner.task_deadline, planner.shift, planner.shift_type))
        self.storage.update_planner(planner.id, new_deadline=planner.task_deadline)

    @log_call
    def _make_task(self, planner):
        """
        Insert new task made by this planner.

        planner -- models.Planner

        """

        self.storage.insert_task(planner.task_name, planner.task_content, STATUS_CREATED,
                                 datetime.now().strftime(self.datetime_format), planner.task_deadline,
                                 planner.task_priority, planner.owner_id, planner.id, None)

    @log_call
    def _plan_first_task(self, planner_id):
        """
        Make new task by this planner withour shifting it.

        planner_id -- int > 0

        """

        planner = self.storage.extract_planner(planner_id)
        self._make_task(planner)

    @log_call
    def _plan_new_task(self, planner_id):
        """
        Execute: extract planner - shift planner - make_task.
        Check the existance of planner.

        planner_id -- int > 0

        """

        planner = self.storage.extract_planner(planner_id)

        if planner is not None:
            self._shift_planner(planner)
            self._make_task(planner)

    def _find_lost_datetimes(self, str_past_moment, str_future_moment, shift, shift_type):
        """
        Find all datetimes between past and future moments. Return list<string>.

        Return array of X[i] in the sequence:
        Past, X[1], ... , X[N], Future,
        where X[i] = Past + i * delta,
        delta is defined by (shift, shift_type)

        str_past_moment -- str like '23.10.2010 13:20'
        str_future_moment -- str in the same format
        shift -- int > 0
        shift_type -- ''hour' or 'hours', 'day' or 'days', 'week' or 'weeks'

        """

        past = datetime.strptime(str_past_moment, self.datetime_format)
        future = datetime.strptime(str_future_moment, self.datetime_format)
        delta = self._make_timedelta(shift, shift_type)

        past += delta  # First shift without adding to list

        lost_times = list()
        while past < future:
            lost_times.append(past.strftime(self.datetime_format))
            past += delta

        return lost_times

    # REGION SHIFTING_AND_DEADLINE_INTERFACE

    @log_call
    def shift_all_reminders(self, user_id):
        """
        For user with id=user_id shift all reminders to the future.

        Return all 'lost' reminders without adding them to the database.
        Returned value format:
        [
            [reminder 1, [array of strings 1]],
            [reminder 2, [array of strings 2]],
            ...
        ]

        user_id -- int > 0

        """

        lost_reminders = list()
        reminders = self.storage.find_all_reminders(user_id)
        if reminders is not None:
            for reminder in reminders:
                if reminder.is_regular() and not self._time_to_remind(reminder):
                    past_time = reminder.invoke_time
                    future_time = self._shift_reminder(reminder)

                    lost_times = self._find_lost_datetimes(past_time, future_time, reminder.shift, reminder.shift_type)
                    lost_reminders.append([reminder, lost_times])

        return lost_reminders

    @log_call
    def fail_all_by_deadline(self, user_id):
        """
        Check all tasks for user with id=user_id and fail
        those which have deadlines in the past.

        user_id -- int > 0

        """

        tasks = self.storage.find_all_tasks(user_id)
        now = datetime.now()

        for task in tasks:
            deadline = datetime.strptime(task.time_deadline, self.datetime_format)
            if task.status != STATUS_DONE and task.status != STATUS_FAILED and deadline <= now:
                self.storage.update_task(task.id, new_status=STATUS_FAILED)

                if task.planner_id is not None:
                    self._plan_new_task(task.planner_id)
