from __future__ import unicode_literals

from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

from argparse import Namespace
from functools import wraps
from logging import (
    DEBUG,
    INFO,
    ERROR,
    FileHandler
)

from .library import interaction
from .library import exceptions
from .library import models
from .library import logging

from .formatters import Formatter
from .forms import (
    AddTaskForm,
    AddPlannerForm,
    AddReminderForm,
    AddRegularReminderForm,
    EditTaskForm,
    EditPlannerForm,
    EditReminderForm,
    UserCreationFormWithEmail
)


DATABASE = 'data.db'
CONTROLLER = interaction.Interaction(DATABASE)

LOGGER = logging.get_logger()
LOGGER.setLevel(DEBUG)
LOGGER.addHandler(FileHandler('finetask.log'))


def require_login(fun):
    @wraps(fun)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return fun(request, *args, **kwargs)
        return redirect('/finetask/login')

    return wrapper


def check_permissions(fun):
    @wraps(fun)
    def wrapper(request, *args, **kwargs):
        try:
            return fun(request, *args, **kwargs)
        except exceptions.PermissionDeniedError as ex:
            template = loader.get_template('finetask/main/no_permission.html')
            return HttpResponse(template.render(
                {
                    'current_user': request.user,
                    'reminders': remind_and_check_deadlines(request)
                }
            ))

    return wrapper


def wrap_error(fun):
    @wraps(fun)
    def wrapper(request, *args, **kwargs):
        try:
            return fun(request, *args, **kwargs)
        except Exception as ex:
            template = loader.get_template('finetask/main/error_occured.html')
            return HttpResponse(template.render(
                {
                    'current_user': request.user,
                    'reminders': remind_and_check_deadlines(request),
                    'exception': str(ex)
                }
            ))

    return wrapper


def signup(request):
    if request.method == 'POST':
        form = UserCreationFormWithEmail(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, request)
            return redirect('index', permanent=True)
    else:
        form = UserCreationFormWithEmail()
    return render(request, 'registration/signup.html', {'form': form})


def remind_and_check_deadlines(request):
    """
    Check reminders for current authorized user and
    fail tasks by deadlines, if it is necessary.

    Return object, containing fields:

    lost_reminders
    current_reminders

    """

    current_user = None

    try:
        current_user = CONTROLLER.get_user_by_name(request.user.username)
    except exceptions.NoUserError:
        return Namespace(lost=None, current=None)

    CONTROLLER.fail_all_by_deadline(current_user.id)
    lost_reminders = CONTROLLER.shift_all_reminders(current_user.id)

    response = Namespace()
    response.lost = [reminder for reminder, lost_datetimes in lost_reminders if lost_datetimes]
    response.current = CONTROLLER.try_to_remind(current_user.id)

    LOGGER.debug('----------call----------')  # To split logs for different pages

    return response


@wrap_error
@require_login
@check_permissions
def index(request):
    template = loader.get_template('finetask/main/index.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def users(request):
    users_list = CONTROLLER.get_all_users()

    template = loader.get_template('finetask/users/all.html')

    return HttpResponse(template.render(
        {
            'users': users_list,
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def tasks(request):
    if request.method == 'POST':
        user = CONTROLLER.get_user_by_name(request.user.username)
        task_id = int(request.POST.get('task_id'))
        CONTROLLER.remove_task(user.id, task_id)

    user = CONTROLLER.get_user_by_name(request.user.username)
    tasks_list = CONTROLLER.get_all_tasks(user.id)

    template = loader.get_template('finetask/tasks/all.html')
    return HttpResponse(template.render(
        {
            'tasks': tasks_list,
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def foreign_tasks(request):
    if request.method == 'POST':
        user = CONTROLLER.get_user_by_name(request.user.username)
        task_id = int(request.POST.get('task_id'))
        CONTROLLER.remove_task(user.id, task_id)

    user = CONTROLLER.get_user_by_name(request.user.username)
    tasks_list = CONTROLLER.get_all_available_tasks(user.id)

    template = loader.get_template('finetask/tasks/foreign.html')
    return HttpResponse(template.render(
        {
            'tasks': tasks_list,
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def planners(request):
    if request.method == 'POST':
        user = CONTROLLER.get_user_by_name(request.user.username)
        planner_id = int(request.POST.get('planner_id'))
        CONTROLLER.remove_planner(user.id, planner_id)

    user = CONTROLLER.get_user_by_name(request.user.username)
    planners_list = CONTROLLER.get_all_planners(user.id)

    template = loader.get_template('finetask/planners/all.html')
    return HttpResponse(template.render(
        {
            'planners': planners_list,
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def reminders(request):
    if request.method == 'POST':
        user = CONTROLLER.get_user_by_name(request.user.username)
        reminder_id = int(request.POST.get('reminder_id'))
        CONTROLLER.remove_reminder(user.id, reminder_id)

    user = CONTROLLER.get_user_by_name(request.user.username)
    reminders_list = CONTROLLER.get_all_reminders(user.id)

    template = loader.get_template('finetask/reminders/all.html')
    return HttpResponse(template.render(
        {
            'reminders_': reminders_list,  # To avoid conflicts with reminders in base.html
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_user(request, user_id):
    template = loader.get_template('finetask/users/show.html')
    return HttpResponse(template.render(
        {
            'user': CONTROLLER.get_user_by_id(user_id),
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_task(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    exception = None

    if request.method == 'POST':
        try:
            status = request.POST.get('status')
            CONTROLLER.change_task(user.id, task_id, status=status)
        except exceptions.InvalidActionError as ex:
            exception = ex

    task = CONTROLLER.get_task_by_id(user.id, task_id)
    formatter = Formatter(CONTROLLER.storage)

    template = loader.get_template('finetask/tasks/show.html')
    return HttpResponse(template.render(
        {
            'task': task,
            'task_formatted': formatter.format_task(task),
            'all_statuses': [
                interaction.STATUS_CREATED,
                interaction.STATUS_IN_PROGRESS,
                interaction.STATUS_DONE,
                interaction.STATUS_FAILED
            ],
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request),
            'exception': exception
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_task_tree(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        detached_id = int(request.POST.get('detached_id'))
        CONTROLLER.detach_task(user.id, detached_id)

    formatter = Formatter(CONTROLLER.storage)

    task = CONTROLLER.get_task_by_id(user.id, task_id)
    tree = formatter.format_task_tree(task)

    template = loader.get_template('finetask/tasks/tree.html')
    return HttpResponse(template.render(
        {
            'tree': tree,
            'task_id': task_id,
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_planner(request, planner_id):
    user = CONTROLLER.get_user_by_name(request.user.username)

    template = loader.get_template('finetask/planners/show.html')
    return HttpResponse(template.render(
        {
            'planner': CONTROLLER.get_planner_by_id(user.id, planner_id),
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_reminder(request, reminder_id):
    user = CONTROLLER.get_user_by_name(request.user.username)

    template = loader.get_template('finetask/reminders/show.html')
    return HttpResponse(template.render(
        {
            'reminder': CONTROLLER.get_reminder_by_id(user.id,  reminder_id),
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_task_groups(request):
    user = CONTROLLER.get_user_by_name(request.user.username)
    groups = CONTROLLER.get_all_groups(user.id)

    template = loader.get_template('finetask/tasks/groups.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'task_groups': groups,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def show_tasks_in_group(request, group_name):
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        task_id = int(request.POST.get('task_id'))
        CONTROLLER.remove_task_from_group(user.id, task_id, group_name)

    tasks = CONTROLLER.get_tasks_by_group_name(user.id, group_name)

    is_editable = group_name not in [interaction.GROUP_ALL, interaction.GROUP_CURRENT, interaction.GROUP_ENDED,
                                     interaction.GROUP_DONE, interaction.GROUP_FAILED]

    template = loader.get_template('finetask/tasks/group_tasks.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'group_name': group_name,
            'group_tasks': tasks,
            'is_editable': is_editable,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def add_task(request):
    form = AddTaskForm()
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        form = AddTaskForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user)
            return redirect('/finetask/tasks', permanent=True)


    template = loader.get_template('finetask/tasks/add.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def add_planner(request):
    form = AddPlannerForm()
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        form = AddPlannerForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user)

            return redirect('/finetask/planners', permanent=True)

    template = loader.get_template('finetask/planners/add.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def add_reminder(request):
    form = AddReminderForm()
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        form = AddReminderForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user)
            return redirect('/finetask/reminders', permanent=True)


    template = loader.get_template('finetask/reminders/add.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def add_regular_reminder(request):
    form = AddRegularReminderForm()
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        form = AddRegularReminderForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user)
            return redirect('/finetask/reminders', permanent=True)

    template = loader.get_template('finetask/reminders/add_regular.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def add_subtask(request, root_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    root_task = CONTROLLER.get_task_by_id(user.id, root_id)
    exception = None

    if request.method == 'POST':
        subtask_id = int(request.POST.get('subtask'))

        try:
            CONTROLLER.attach_task(user.id, subtask_id, root_id)
            return redirect('/finetask/tasks/{id}'.format(id=root_id))
        except exceptions.InvalidActionError as ex:
            exception = ex

    all_tasks = CONTROLLER.get_all_tasks(user.id, skip_archieve=False)
    formatter = Formatter(CONTROLLER.storage)

    template = loader.get_template('finetask/tasks/attach.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'task': root_task,
            'task_formatted': formatter.format_task(root_task),
            'subtasks': all_tasks,
            'exception': exception,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def edit_dependency(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    main_task = CONTROLLER.get_task_by_id(user.id, task_id)
    exception = None

    behaviours = (
        interaction.BEHAVIOUR_NONE,
        interaction.BEHAVIOUR_DIRECT,
        interaction.BEHAVIOUR_INVERSE,
        interaction.BEHAVIOUR_PARALLEL
    )

    if request.method == 'POST':
        main_id = int(request.POST.get('main_task'))
        behaviour = request.POST.get('behaviour')

        assert behaviour in behaviours  # Not to write "asldfjgehgr" behaviour

        if behaviour == interaction.BEHAVIOUR_NONE:  # To remove dependency
            try:
                CONTROLLER.remove_dependency(user.id, task_id)
            except exceptions.InvalidActionError as ex:
                exception = ex

        else:  # To add dependency
            try:
                dependency = models.Dependency(task_id, main_id, behaviour, user.id)
                CONTROLLER.add_dependency(user.id, dependency)
            except exceptions.InvalidActionError as ex:
                exception = ex

    all_tasks = CONTROLLER.get_all_tasks(user.id, skip_archieve=False)

    formatter = Formatter(CONTROLLER.storage)

    template = loader.get_template('finetask/tasks/dependency.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'task': main_task,
            'task_formatted': formatter.format_task(main_task),
            'all_tasks': all_tasks,
            'behaviours': behaviours,
            'exception': exception,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def edit_permissions(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    exception = None

    if request.method == 'POST':
        try:
            level = request.POST.get('permission')
            guest_id = int(request.POST.get('guest'))
            permission = models.Permission(user.id, guest_id, task_id, level)
            CONTROLLER.add_permission(user.id, permission)
        except exceptions.InvalidActionError as ex:
            exception = ex

    task = CONTROLLER.get_task_by_id(user.id, task_id)
    formatter = Formatter(CONTROLLER.storage)

    template = loader.get_template('finetask/tasks/permissions.html')
    return HttpResponse(template.render(
        {
            'task': task,
            'task_formatted': formatter.format_task(task),
            'all_levels': [
                interaction.PERMISSION_READ,
                interaction.PERMISSION_CONFIRM,
                interaction.PERMISSION_EDIT
            ],
            'all_users': CONTROLLER.get_all_users(),
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request),
            'exception': exception
        }
    ))


@wrap_error
@require_login
@check_permissions
def move_to_group(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    exception = None

    if request.method == 'POST':
        try:
            group_name = request.POST.get('group_name')
            CONTROLLER.move_task_to_group(user.id, task_id, group_name)
            return redirect('/finetask/tasks/{id}'.format(id=task_id))
        except exceptions.InvalidActionError as ex:
            exception = ex

    task = CONTROLLER.get_task_by_id(user.id, task_id)
    formatter = Formatter(CONTROLLER.storage)

    template = loader.get_template('finetask/tasks/group_move.html')
    return HttpResponse(template.render(
        {
            'task': task,
            'task_formatted': formatter.format_task(task),
            'current_user': request.user,
            'reminders': remind_and_check_deadlines(request),
            'exception': exception
        }
    ))


@wrap_error
@require_login
@check_permissions
def edit_task(request, task_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    form = EditTaskForm()

    if request.method == 'POST':
        form = EditTaskForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user, task_id)
            return redirect('/finetask/tasks', permanent=True)

    form.set_initials(CONTROLLER, form, user, task_id)
    template = loader.get_template('finetask/tasks/edit.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'task_id': task_id,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def edit_planner(request, planner_id):
    user = CONTROLLER.get_user_by_name(request.user.username)
    form = EditPlannerForm()

    if request.method == 'POST':
        form = EditPlannerForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user, planner_id)
            return redirect('/finetask/planners', permanent=True)

    form.set_initials(CONTROLLER, form, user, planner_id)
    template = loader.get_template('finetask/planners/edit.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'planner_id': planner_id,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))


@wrap_error
@require_login
@check_permissions
def edit_reminder(request, reminder_id):
    form = EditReminderForm()
    user = CONTROLLER.get_user_by_name(request.user.username)

    if request.method == 'POST':
        form = EditReminderForm(request.POST)
        if form.is_valid():
            form.process(CONTROLLER, form, user, reminder_id)
            return redirect('/finetask/reminders', permanent=True)

    form.set_initials(CONTROLLER, form, user, reminder_id)
    template = loader.get_template('finetask/reminders/edit.html')
    return HttpResponse(template.render(
        {
            'current_user': request.user,
            'reminder_id': reminder_id,
            'form': form,
            'reminders': remind_and_check_deadlines(request)
        }
    ))
