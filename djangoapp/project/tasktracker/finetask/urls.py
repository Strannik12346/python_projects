from django.conf.urls import include, url

from . import views
from django.contrib.auth import views as auth_views
from django.contrib import admin


urlpatterns = [
    url(r'^users/', include('finetask.users.urls')),
    url(r'^tasks/', include('finetask.tasks.urls')),
    url(r'^planners/', include('finetask.planners.urls')),
    url(r'^reminders/', include('finetask.reminders.urls')),
    url(r'^$', views.index, name='index'),

    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^admin/', admin.site.urls),
]
