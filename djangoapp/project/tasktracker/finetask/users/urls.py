from django.conf.urls import url

from finetask import views


urlpatterns = [
    url(r'^(?P<user_id>[0-9]*)/$', views.show_user),
    url(r'^$', views.users),
]