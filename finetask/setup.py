from setuptools import setup
from setuptools import find_packages


setup(
    name='FineTask',
    version='1.0',
    packages=find_packages(),
    url='',
    license='',
    author='Strannik12346',
    author_email='strannik277@gmail.com',
    description='FineTask Application',
    entry_points={
        'console_scripts':
            [
                'finetask=finetask.console.main:main'
            ]
    },
    install_requires=['sqlalchemy'],
    include_package_data=True
)