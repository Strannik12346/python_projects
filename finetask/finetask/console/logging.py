from finetask.library.logging import get_logger


def log_args(args):
    arr = []
    for key, value in vars(args).items():
        if value is not None:
            arr.append('[--{key} : {value}]'.format(key=str(key), value=str(value)))
    arr.append('\n')

    logger = get_logger()
    logger.info('\n'.join(arr))