"""
All arguments are defined here.

Do not define your arguments constants,
just import them from this file.

"""


ARGS_ID = 'id'
ARGS_NICNAME = 'nickname'
ARGS_EMAIL = 'email'
ARGS_RATE = 'rate'
ARGS_ALL = 'all'

ARGS_NAME = 'name'
ARGS_CONTENT = 'content'
ARGS_DEADLINE = 'deadline'
ARGS_STATUS = 'status'
ARGS_PRIORITY = 'priority'

ARGS_ROOT = 'root'
ARGS_TREE = 'tree'

ARGS_GROUP = 'group'
ARGS_OUT = 'out'

ARGS_MEMBER = 'member'
ARGS_LEVEL = 'level'
ARGS_EXPIRE = 'expire'

ARGS_TEXT = 'text'
ARGS_TIME = 'time'
ARGS_PLANNING = 'planning'

ARGS_FIRST = 'first'
ARGS_SECOND = 'second'
ARGS_BEHAVIOUR = 'behaviour'
