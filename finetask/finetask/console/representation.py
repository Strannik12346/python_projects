from datetime import datetime
from functools import wraps

from finetask.console import validation
from finetask.console.arguments import (
    ARGS_ID,
    ARGS_NICNAME,
    ARGS_EMAIL,
    ARGS_RATE,
    ARGS_ALL,
    ARGS_NAME,
    ARGS_CONTENT,
    ARGS_DEADLINE,
    ARGS_STATUS,
    ARGS_PRIORITY,
    ARGS_ROOT,
    ARGS_TREE,
    ARGS_GROUP,
    ARGS_OUT,
    ARGS_MEMBER,
    ARGS_LEVEL,
    ARGS_EXPIRE,
    ARGS_TEXT,
    ARGS_TIME,
    ARGS_PLANNING,
    ARGS_FIRST,
    ARGS_SECOND,
    ARGS_BEHAVIOUR
)

from finetask.library import models
from finetask.library.logging import get_logger
from finetask.library.exceptions import InvalidActionError
from finetask.library.interaction import Interaction
from finetask.library.interaction import STATUS_CREATED
from finetask.console.formatting import Formatter


def print_iterable(iterable, skip_none=True, as_table=False):
    max_length = 0
    sequence = []

    for item in iterable:
        if (skip_none and item is not None) or (not skip_none):
            string = str(item)
            max_length = max(len(string), max_length)
            sequence.append(string)

    if as_table:
        separator = '-' * max_length
        sequence = [separator] + sequence + [separator]

    result = "\n".join(sequence)
    print(result)


def except_print_message(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        try:
            fun(*args, **kwargs)
        except InvalidActionError as ex:
            logger = get_logger()
            logger.error(ex)
            print(ex.message)

    return wrapper


class Representation:
    def __init__(self, filename):
        self.controller = Interaction(filename)
        self.formatter = Formatter(self.controller.storage)

    # REGION PRE-ACTIONS

    def validate_all(self, args):
        if not validation.validate_args(args):
            print('Validation failed. Args must be in different format.')
            return False
        return True

    @except_print_message
    def remind_all(self, args):
        lost_reminders = self.controller.shift_all_reminders(args.owner_id)
        lost_formatted_reminders = self.formatter.format_lost_reminders(lost_reminders)
        if lost_formatted_reminders:
            print_iterable(lost_formatted_reminders)

        current_reminders = ['Your current reminders:'] + self.controller.try_to_remind(args.owner_id)
        print_iterable(current_reminders, as_table=True)

    @except_print_message
    def fail_all_deadlines(self, args):
        self.controller.fail_all_by_deadline(args.owner_id)

    # REGION USERS

    def _show_one_user(self, args):
        user = self.controller.get_user_by_id(args.id)
        print(user)

    def _show_user_owner(self, args):
        user = self.controller.get_user_by_id(args.owner_id)
        print(user)

    def _show_all_users(self, args):
        response = self.controller.get_all_users()
        print_iterable(response, as_table=True)

    @except_print_message
    def show_user(self, args):  # By default shows owner
        if validation.require_all(args, [ARGS_ID]):
            self._show_one_user(args)
        elif validation.require_all(args, [ARGS_ALL]):
            self._show_all_users(args)
        else:
            self._show_user_owner(args)

    @except_print_message
    def add_user(self, args):
        if validation.require_all(args, [ARGS_NICNAME, ARGS_EMAIL]):
            new_user = models.User(args.nickname, args.email, 0)
            new_user_id = self.controller.add_user(new_user)
            print('New user received ID #{id}'.format(id=new_user_id))
        else:
            print(validation.whats_missed(args, [ARGS_NICNAME, ARGS_EMAIL]))

    @except_print_message
    def change_user(self, args):  # We can change only owner
        if validation.require_any(args, [ARGS_NICNAME, ARGS_EMAIL, ARGS_RATE]):
            text_responses = [
                'Successfully updated nickname of user #{id}'.format(id=args.owner_id),
                'Successfully updated email of user #{id}'.format(id=args.owner_id),
                'Successfully updated rate of user #{id}'.format(id=args.owner_id)
            ]

            bool_responses = [
                args.nickname is not None,
                args.email is not None,
                args.rate is not None
            ]

            if self.controller.change_user(args.owner_id, args.nickname, args.email, args.rate):
                for was_updated, message in zip(bool_responses, text_responses):
                    if was_updated:
                        print(message)
        else:
            print(validation.whats_missed(args, [ARGS_NICNAME, ARGS_EMAIL, ARGS_RATE]))

    @except_print_message
    def remove_user(self, args):
        if validation.require_all(args, [ARGS_ID]):
            if self.controller.remove_user(args.owner_id, args.id):
                print('User #{id} deleted.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    # REGION TASKS

    def _show_one_task(self, args):
        task = self.controller.get_task_by_id(args.owner_id, args.id)
        response = self.formatter.format_task(task)
        print(response)

    def _show_all_tasks(self, args):
        response = self.controller.get_all_tasks(args.owner_id)
        print_iterable(response, as_table=True)

    def _show_task_tree(self, args):
        root_task = self.controller.get_task_by_id(args.owner_id, args.root)
        response = self.formatter.format_task_tree(root_task)
        print(response)

    def _show_tasks_in_group(self, args):
        response = [args.group + ':'] + self.controller.get_tasks_by_group_name(args.owner_id, args.group)
        print_iterable(response, as_table=True)

    @except_print_message
    def show_task(self, args):
        arguments = {(ARGS_ID,): self._show_one_task,
                     (ARGS_ALL,): self._show_all_tasks,
                     (ARGS_TREE, ARGS_ROOT): self._show_task_tree,
                     (ARGS_GROUP,): self._show_tasks_in_group
                     }

        for key, funct in arguments.items():
            if validation.require_all(args, key):
                funct(args)
                break
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_ALL, ARGS_TREE, ARGS_ROOT, ARGS_GROUP]))

    @except_print_message
    def add_task(self, args):
        str_now = datetime.now().strftime(self.controller.datetime_format)

        if validation.require_all(args, [ARGS_ROOT, ARGS_NAME, ARGS_CONTENT, ARGS_DEADLINE]):  # Trying to add subtask
            priority = validation.parse_priority(args.priority) if args.priority is not None else 1
            new_subtask = models.Task(args.name, args.content, STATUS_CREATED, str_now, args.deadline,
                                      priority, args.owner_id, parent_id=args.root)
            new_subtask_id = self.controller.add_task(new_subtask)
            print('New task received ID #{id}'.format(id=new_subtask_id))
        elif validation.require_all(args, [ARGS_NAME, ARGS_CONTENT, ARGS_DEADLINE]):  # Then trying to add simple task
            priority = validation.parse_priority(args.priority) if args.priority is not None else 1
            new_task = models.Task(args.name, args.content, STATUS_CREATED,
                                   str_now, args.deadline, priority, args.owner_id)
            new_task_id = self.controller.add_task(new_task)
            print('New task received ID #{id}'.format(id=new_task_id))
        else:
            print(validation.whats_missed(args, [ARGS_NAME, ARGS_CONTENT, ARGS_DEADLINE]))

    @except_print_message
    def change_task(self, args):
        if (validation.require_all(args, [ARGS_ID]) and
                validation.require_any(args, [ARGS_NAME, ARGS_CONTENT, ARGS_STATUS, ARGS_PRIORITY])):

            text_responses = [
                'Successfully updated name of the task #{id}'.format(id=args.id),
                'Successfully updated content of the task #{id}'.format(id=args.id),
                'Successfully updated status of the task #{id}'.format(id=args.id),
                'Successfully updated priority of the task #{id}'.format(id=args.id)
            ]

            bool_responses = [
                args.name is not None,
                args.content is not None,
                args.status is not None,
                args.priority is not None
            ]

            if self.controller.change_task(args.owner_id, args.id, args.name,
                                           args.content, args.status, validation.parse_priority(args.priority)):
                for was_changed, message in zip(bool_responses, text_responses):
                    if was_changed:
                        print(message)
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_NAME, ARGS_CONTENT, ARGS_STATUS, ARGS_PRIORITY]))

    @except_print_message
    def attach_task(self, args):
        if validation.require_all(args, [ARGS_ID, ARGS_ROOT]):
            self.controller.attach_task(args.owner_id, args.id, args.root)
            print('Successfully attached task #{id} to the root #{root_id}.'.format(id=args.id, root_id=args.root))
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_ROOT]))

    @except_print_message
    def detach_task(self, args):
        if validation.require_all(args, [ARGS_ID]):
            self.controller.detach_task(args.owner_id, args.id)
            print('Successfully detached task #{id} if it had ever had parent.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    @except_print_message
    def remove_task(self, args):
        if validation.require_all(args, [ARGS_ID]):
            if self.controller.remove_task(args.owner_id, args.id):
                print('Task #{id} deleted.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    def _move_task_to_group(self, args):
        if self.controller.move_task_to_group(args.id, args.group):
            print('Successfully added task #{id} to group {name}'.format(id=args.id, name=args.group))

    def _move_task_from_group(self, args):
        if self.controller.remove_task_from_group(args.id, args.group):
            print('Successfully removed task #{id} from group {name}'.format(id=args.id, name=args.group))

    @except_print_message
    def move_task(self, args):
        if validation.require_all(args, [ARGS_ID, ARGS_GROUP]):
            if validation.require_all(args, [ARGS_OUT]):
                self._move_task_from_group(args)
            else:
                self._move_task_to_group(args)
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_GROUP]))

    @except_print_message
    def show_task_groups(self, args):
        response = self.controller.get_all_groups(args.owner_id)
        print_iterable(response, as_table=True)

    # REGION PLANNERS

    @except_print_message
    def _show_one_planner(self, args):
        planner = self.controller.get_planner_by_id(args.owner_id, args.id)
        print(planner)

    def _show_all_planners(self, args):
        response = self.controller.get_all_planners(args.owner_id)
        print_iterable(response, as_table=True)

    @except_print_message
    def show_planner(self, args):
        if validation.require_all(args, [ARGS_ALL]):
            self._show_all_planners(args)
        elif validation.require_all(args, [ARGS_ID]):
            self._show_one_planner(args)
        else:
            print(validation.whats_missed(args, [ARGS_ALL, ARGS_ID]))

    @except_print_message
    def add_planner(self, args):
        if validation.require_all(args, [ARGS_NAME, ARGS_CONTENT, ARGS_PLANNING]):
            time, shift, shift_type = validation.parse_planning(args.planning)
            priority = validation.parse_priority(args.priority) if args.priority is not None else 1
            new_planner = models.Planner(args.name, args.content, time, priority,
                                         shift, shift_type, args.owner_id)
            new_planner_id = self.controller.add_planner(new_planner)
            print('New planner received ID #{id}'.format(id=new_planner_id))
        else:
            print(validation.whats_missed(args, [ARGS_NAME, ARGS_CONTENT, ARGS_PLANNING, ARGS_PRIORITY]))

    @except_print_message
    def change_planner(self, args):
        if validation.require_all(args, [ARGS_ID]) and validation.require_any(args, [ARGS_NAME, ARGS_CONTENT, ARGS_PRIORITY]):
            text_responses = [
                'Successfully updated name of the planner #{id}'.format(id=args.id),
                'Successfully updated content of the planner #{id}'.format(id=args.id),
                'Successfully updated priority of the planner #{id}'.format(id=args.id)
            ]

            bool_responses = [
                args.name is not None,
                args.content is not None,
                args.priority is not None
            ]

            if self.controller.change_planner(args.owner_id, args.id, args.name, args.content, args.priority):
                for was_changed, message in zip(bool_responses, text_responses):
                    if was_changed:
                        print(message)
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_NAME, ARGS_CONTENT, ARGS_PRIORITY]))

    @except_print_message
    def remove_planner(self, args):
        if validation.require_all(args, [ARGS_ID]):
            if self.controller.remove_planner(args.owner_id, args.id):
                print('Planner #{id} deleted.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    # REGION REMINDERS

    def _show_one_reminder(self, args):
        reminder = self.controller.get_reminder_by_id(args.owner_id, args.id)
        print(reminder)

    def _show_all_reminders(self, args):
        response = self.controller.get_all_reminders(args.owner_id)
        print_iterable(response, as_table=True)

    @except_print_message
    def show_reminder(self, args):
        if validation.require_all(args, [ARGS_ALL]):
            self._show_all_reminders(args)
        elif validation.require_all(args, [ARGS_ID]):
            self._show_one_reminder(args)
        else:
            print(validation.whats_missed(args, [ARGS_ALL, ARGS_ID]))

    @except_print_message
    def add_reminder(self, args):
        if validation.require_all(args, [ARGS_TEXT, ARGS_TIME]):  # Trying to add simple reminder
            new_reminder = models.Reminder(args.text, args.time, args.owner_id)
            new_reminder_id = self.controller.add_reminder(new_reminder)
            print('New reminder received ID #{id}'.format(id=new_reminder_id))

        elif validation.require_all(args, [ARGS_TEXT, ARGS_PLANNING]):  # Then regular reminder
            pformat = validation.parse_planning(args.planning)
            new_reminder = models.Reminder(args.text, pformat[0], args.owner_id, pformat[1], pformat[2])
            new_reminder_id = self.controller.add_reminder(new_reminder)
            print('New reminder received ID #{id}'.format(id=new_reminder_id))

        else:
            print(validation.whats_missed(args, [ARGS_TEXT, ARGS_TIME, ARGS_PLANNING]))

    @except_print_message
    def change_reminder(self, args):
        if validation.require_all(args, [ARGS_ID]) and validation.require_any(args, [ARGS_TEXT, ARGS_TIME]):
            text_responses = [
                'Successfully updated text of the reminder #{id}'.format(id=args.id),
                'Successfully updated invoke time of the reminder #{id}'.format(id=args.id)
            ]

            bool_responses = [
                args.text is not None,
                args.time is not None
            ]

            if self.controller.change_reminder(args.owner_id, args.id, args.text, args.time):
                for was_changed, message in zip(bool_responses, text_responses):
                    if was_changed:
                        print(message)
        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_TEXT, ARGS_TIME]))

    @except_print_message
    def remove_reminder(self, args):
        if validation.require_all(args, [ARGS_ID]):
            if self.controller.remove_reminder(args.owner_id, args.id):
                print('Reminder #{id} deleted.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    # REGION DEPENDENCIES

    @except_print_message
    def show_dependency(self, args):
        if validation.require_all(args, [ARGS_ID]):
            dependency = self.controller.get_dependency_by_task_id(args.owner_id, args.id)
            print(dependency)
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    @except_print_message
    def add_dependency(self, args):
        if validation.require_all(args, [ARGS_FIRST, ARGS_SECOND, ARGS_BEHAVIOUR]):
            new_dependency = models.Dependency(args.first, args.second, args.behaviour, args.owner_id)
            new_dependency_id = self.controller.add_dependency(new_dependency)
            print('New dependency received ID #{id}'.format(id=new_dependency_id))
        else:
            print(validation.whats_missed(args, [ARGS_FIRST, ARGS_SECOND, ARGS_BEHAVIOUR]))

    @except_print_message
    def remove_dependency(self, args):
        if validation.require_all(args, [ARGS_ID]):
            if self.controller.remove_dependency(args.owner_id, args.id):
                print('Task #{id} is no more dependant.'.format(id=args.id))
        else:
            print(validation.whats_missed(args, [ARGS_ID]))

    # REGION ACCESS

    @except_print_message
    def manage_permissions(self, args):
        if validation.require_all(args, [ARGS_ID, ARGS_MEMBER, ARGS_LEVEL]):
            access = models.Permission(args.owner_id, args.member, args.id, args.level)
            if self.controller.add_permission(args.owner_id, access):
                print('User #{guest} got access to the task #{task} with level {level}.'.format(
                    guest=args.member, task=args.id, level=args.level))

        elif validation.require_all(args, [ARGS_ID, ARGS_MEMBER, ARGS_EXPIRE]):
            if self.controller.remove_permission(args.owner_id, args.member, args.id):
                print('Rights for guest #{id} deleted.'.format(id=args.member))

        else:
            print(validation.whats_missed(args, [ARGS_ID, ARGS_MEMBER, ARGS_LEVEL, ARGS_EXPIRE]))
