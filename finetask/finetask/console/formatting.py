from finetask.library.models import User
from finetask.library.models import Task
from finetask.library.models import Planner
from finetask.library.models import Reminder
from finetask.library.models import Dependency
from finetask.library.logging import log_call


class Formatter:
    def __init__(self, storage):
        self.storage = storage

    @log_call
    def format_user(self, user):
        """Return string representation of user. If user is string message, return it."""

        return str(user) if type(user) == User else user

    @log_call
    def format_task(self, task):
        """Return string representation of task. If task is string message, return it."""

        if isinstance(task, Task):
            owner = self.storage.extract_user(task.owner_id)
            if owner is not None:
                owner = owner.short_str()

            planner = self.storage.extract_planner(task.planner_id)
            if planner is not None:
                planner = planner.short_str()

            parent = self.storage.extract_task(task.parent_id)
            if parent is not None:
                parent = str(parent)

            dependency = self.storage.extract_dependency(task.id)
            if dependency is not None:
                dependency = dependency.short_str()

            main_properties = task.to_str_list()
            additional_properties = ['Owned by user {user}'.format(user=owner),
                                     'Planner: {plr}'.format(plr=planner),
                                     'Parent: {pnt}'.format(pnt=parent),
                                     'Depends on: {dep}'.format(dep=dependency)]
            access_list = self.storage.find_all_granted_permissions(task.id)
            access_property = ['Access:'] + list(map(str, access_list))

            seq = main_properties + additional_properties + access_property
            return '\n'.join(seq)

        else:
            return task

    @log_call
    def format_task_tree(self, task, tabs=''):
        """
        Return task tree in the following format:

        * task1
            * subtask1
                * sub-subtask11
                * sub-subtask12
            * subtask2
                * sub-subtask21
                * sub-subtask22

        task -- models.Task

        """

        response = str()
        response += '{align}* {task}'.format(align=tabs, task=str(task)) + "\n"

        for task in self.storage.find_subtasks(task.id):
            response += self.format_task_tree(task, tabs=tabs + "\t")

        return response

    @log_call
    def format_planner(self, planner):
        """Return string representation of planner. If planner is string message, return it."""

        return str(planner) if isinstance(planner, Planner) else planner

    @log_call
    def format_reminder(self, reminder):
        """Return string representation of reminder. If reminder is string message, return it."""

        return str(reminder) if isinstance(reminder, Reminder) else reminder

    @log_call
    def format_lost_reminder(self, reminder, lost_datetimes):
        """Return string representation of the reminder missed by the user."""

        return 'Reminder #{id} with text "{text}" was missed {count} times.\nThe last time missed was {last_missed}.'.format(
            id=reminder.id,
            count=len(lost_datetimes),
            text=reminder.text,
            last_missed=lost_datetimes[-1]
        )

    @log_call
    def format_lost_reminders(self, lost_reminders):
        """
        Format list of lost reminders. Return list<string>

        Used to represent old values of reminder datetimes
        that were not used in the result of user absence.

        lost_reminders -- list in the following format:
        [
            [reminder 1, [list of strings 1]],
            [reminder 2, [list of strings 2]],
            ...
        ]

        """

        str_results = list()
        for reminder, lost_datetimes in lost_reminders:
            if lost_datetimes:
                str_results.append('!!! -- MISSED REMINDER -- !!!')
                str_results.append(self.format_lost_reminder(reminder, lost_datetimes))

        return str_results

    @log_call
    def format_dependency(self, dependency):
        """Return string representation of dependency. If dependency is string message, return it."""

        return str(dependency) if isinstance(dependency, Dependency) else dependency
