import functools
import logging
import json
import os

from finetask.library.logging import get_logger
from finetask.library.interaction import Interaction


APPLICATION_NAME = 'finetask'
USER_FILE = 'user.json'

DEFAULT_DATABASE = 'data.db'
DEFAULT_LOG_LEVEL = 'error'
DEFAULT_LOG_FILE = 'finetask.log'
DEFAULT_USER = 'root'

LOGGING_DEBUG = 'DEBUG'
LOGGING_INFO = 'INFO'
LOGGING_ERROR = 'ERROR'
LOGGING_OFF = 'OFF'


def false_if_exception(fun):
    @functools.wraps(fun)
    def wrapper(*args, **kwargs):
        try:
            return fun(*args, **kwargs)
        except Exception:
            return False

    return wrapper


def set_database(args, database):
    """
    Set args.database.

    args -- argparse.Namespace object
    database -- str, existing filename

    """

    args.database = database


def set_log_level(args, str_level):
    """
    Set args.log_level using str_level.

    args -- argparse.Namespace object
    str_level -- 'debug', 'info', 'error', 'critical', 'off'

    """

    levels = {
        LOGGING_DEBUG: logging.DEBUG,
        LOGGING_INFO: logging.INFO,
        LOGGING_ERROR: logging.ERROR,
        LOGGING_OFF: logging.CRITICAL
    }

    key = str_level.upper()
    args.log_level = levels[key] if key in levels.keys() else logging.INFO

    logger = get_logger()
    logger.setLevel(args.log_level)


def set_log_file(args, filename):
    """
    Set args.log_file using filename and add file handler to our logger.

    args -- argparse.Namespace object
    filename -- file to log into

    """

    args.log_file = filename
    logger = get_logger()
    logger.addHandler(logging.FileHandler(args.log_file))


def set_owner(args, database, name):
    """
    If name exists in the database, set args.owner_id = user_id(name). Return bool.

    args -- argparse.Namespace object
    database -- str, filename
    name -- str, existing username

    """

    controller = Interaction(database)
    user = controller.get_user_by_name(name)

    if user is not None:
        args.owner_id = user.id
        return True
    else:
        return False


def set_config(args, data_file, username, log_level=LOGGING_OFF, log_file=None):
    """
    Add several attributes to args object.

    if log_file is not None, configurate logger.

    args.database = data_file [necessary]
    args.owner_id = user_id(username) [necessary]
    args.log_level = log_level [optional]
    args.log_file = log_file [oprtional]

    data_file -- str, existing filename
    username -- str, existing user
    log_level -- str, 'debug', 'info', 'error', 'critical', 'off'
    log_file -- str, filename

    """

    set_database(args, data_file)
    set_log_level(args, log_level)

    if log_level.upper() != LOGGING_OFF and log_file is not None:
        set_log_file(args, log_file)

    return set_owner(args, data_file, username)


@false_if_exception
def set_config_with_user(args, username):
    """
    Set config if --user was specified. The rest arguments are default.

    args -- argparse.Namespace object
    username -- str, existing user

    """

    home_dir = os.getenv('HOME')
    data_file = os.path.join(home_dir, APPLICATION_NAME, DEFAULT_DATABASE)

    return set_config(args, data_file, username)


@false_if_exception
def set_config_from_file(args, config_file_name):
    """
    Set config if --config was specified. Take arguments from config_file_name.

    args -- argparse.Namespace object
    config_file_name -- str, existing file

    """

    with open(config_file_name, "r") as file:
        json_obj = json.loads(file.read())

    data_file = json_obj['database']
    username = json_obj['name']
    log_level = json_obj['logging']
    log_file = json_obj['log']

    return set_config(args, data_file, username, log_level=log_level, log_file=log_file)


@false_if_exception
def set_config_from_default_location(args):
    """
    Set config using $HOME/finetask/user.json.

    args -- argparse.Namespace object

    """

    home_dir = os.getenv('HOME')
    user_file = os.path.join(home_dir, APPLICATION_NAME, USER_FILE)

    return set_config_from_file(args, user_file)


@false_if_exception
def set_default_config(args):
    """
    Set default config to args. Make default config file in $HOME/finetask.

    args -- argparse.Namespace object

    """

    home_dir = os.getenv('HOME')

    data_file = os.path.join(home_dir, APPLICATION_NAME, DEFAULT_DATABASE)
    log_file = os.path.join(home_dir, APPLICATION_NAME, DEFAULT_LOG_FILE)

    finetask_dir = os.path.join(home_dir, APPLICATION_NAME)
    user_file = os.path.join(home_dir, APPLICATION_NAME, USER_FILE)

    user_data = json.dumps(
        {
            'database': data_file,
            'name': DEFAULT_USER,
            'logging': DEFAULT_LOG_LEVEL,
            'log': log_file,
        },
        sort_keys=True, indent=4
    )

    if not os.path.exists(finetask_dir):
        os.mkdir(finetask_dir)

    with open(user_file, 'w') as file:
        file.write(user_data)

    return set_config(args, data_file, DEFAULT_USER, DEFAULT_LOG_LEVEL, log_file)
