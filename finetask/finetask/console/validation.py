from datetime import datetime

import finetask.library.interaction as interaction
from finetask.library.models import Task
from finetask.console.arguments import (
    ARGS_DEADLINE,
    ARGS_TIME,
    ARGS_STATUS,
    ARGS_PLANNING,
    ARGS_BEHAVIOUR,
    ARGS_LEVEL,
    ARGS_PRIORITY
)


def require_all(args, names):
    """
    Check all names as attributes of args obj.

    Return True only if all of them are not None.

    args -- argparse.Namespace
    names -- iterable<str>

    """

    for name in names:
        if args.__dict__[name] is None:
            return False
    return True


def require_any(args, names):
    """
    Check all names as attribute of args obj.

    Return True if at least one of them is not None.

    args -- argparse.Namespace
    names -- iterable<str>

    """

    for name in names:
        if args.__dict__[name] is not None:
            return True
    return False


def whats_missed(args, names):
    """
    Find required args attributes which are None. Return str.

    Check args.__dict__ using keys provided in
    names iterable. Return string response.

    args -- argparse.Namespace
    names -- iterable<str>

    """

    missed = []
    for name in names:
        if args.__dict__[name] is None:
            missed.append('"{missed_arg}"'.format(missed_arg=name))
    return 'Arguments ' + ', '.join(missed) + ' are None'


def validate_datetime(str_datetime):
    """
    Return True if datetime is in proper format.

    str_datetime -- datetime in format '23.09.2000 12:20.

    """

    try:
        datetime.strptime(str_datetime, interaction.DATETIME_FORMAT)
        return True
    except:
        return False


def validate_status(status):
    """
    Return True if status is in proper format.

    status -- 'Created', 'In Progress', 'Done' or 'Failed'

    """

    return status in [interaction.STATUS_CREATED, interaction.STATUS_IN_PROGRESS,
                      interaction.STATUS_DONE, interaction.STATUS_FAILED]


def validate_planning(str_planning):
    """
    Validate str_planning as "[datetime] [number] [unit]" and return True/False.

    datetime -- str in format: "11.10.2026 17:25"
    number -- int > 0
    unit -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week'

    """

    try:
        str_date, str_time, shift, shift_type = str_planning.split(' ')
        str_datetime = str_date + " " + str_time

        datetime.strptime(str_datetime, interaction.DATETIME_FORMAT)

        return int(shift) > 0 and shift_type in [interaction.SHIFT_TYPE_HOURS, interaction.SHIFT_TYPE_HOUR, interaction.SHIFT_TYPE_DAYS,
                                                 interaction.SHIFT_TYPE_DAY, interaction.SHIFT_TYPE_WEEKS, interaction.SHIFT_TYPE_WEEK]
    except:
        return False


def validate_behaviour(behaviour):
    """
    Return True if behaviour is in proper format.

    behaviour -- 'Direct', 'Inverse' or 'Parallel'

    """

    return behaviour in [interaction.BEHAVIOUR_DIRECT,
                         interaction.BEHAVIOUR_INVERSE,
                         interaction.BEHAVIOUR_PARALLEL]


def validate_permissions(level):
    """
    Return True if access level is in proper format.

    level -- 'can_read', 'can_confirm', 'can_edit'

    """

    return level in [interaction.PERMISSION_READ,
                     interaction.PERMISSION_CONFIRM,
                     interaction.PERMISSION_EDIT]


def validate_priority(priority):
    """Return True if priority is int number or 'High', 'Medium' or 'Low'."""

    try:
        int(priority)
        return True
    except:
        return priority in Task.IMPORTANCE_LEVEL_TO_INT.keys()


def validate_args(args):
    """
    Check args attributes using the functions above.

    Before checking provide None to the non-existing attributes
    of the args object. Check attributes: deadline, time, status,
    planning, behaviour, level, priority.

    args -- argparse.Namespace object.

    """

    for attr in [ARGS_DEADLINE, ARGS_TIME, ARGS_STATUS, ARGS_PLANNING,
                 ARGS_BEHAVIOUR, ARGS_LEVEL, ARGS_PRIORITY]:
        try:
            args.__dict__[attr]
        except:
            args.__dict__[attr] = None

    return not ((args.deadline is not None and not validate_datetime(args.deadline)) or
                (args.time is not None and not validate_datetime(args.time)) or
                (args.status is not None and not validate_status(args.status)) or
                (args.planning is not None and not validate_planning(args.planning)) or
                (args.behaviour is not None and not validate_behaviour(args.behaviour)) or
                (args.level is not None and not validate_permissions(args.level)) or
                (args.priority is not None and not validate_priority(args.priority)))


def parse_priority(str_priority):
    """Return int representation of priority given. For None provide None."""

    return Task.get_int_priority(str_priority) if str_priority is not None else None


def parse_planning(str_planning):
    """
    Parse str_planning as "[datetime] [number] [unit]" and return a list. On any error return None.

    datetime -- str in format: "11.10.2026 17:25"
    number -- int > 0
    unit -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week'

    """

    try:
        str_date, str_time, str_shift, shift_type = str_planning.split(' ')
        str_datetime = str_date + " " + str_time

        datetime.strptime(str_datetime, interaction.DATETIME_FORMAT)  # Validating datetime

        if int(str_shift) > 0 and shift_type in [interaction.SHIFT_TYPE_HOURS, interaction.SHIFT_TYPE_HOUR, interaction.SHIFT_TYPE_DAYS,
                                                 interaction.SHIFT_TYPE_DAY, interaction.SHIFT_TYPE_WEEKS, interaction.SHIFT_TYPE_WEEK]:

            return [str_datetime, int(str_shift), shift_type]

        return None

    except:
        return None
