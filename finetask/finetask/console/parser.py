ARGS_ENTITY = 'entity'

USER_PARSER = 'user'
TASK_PARSER = 'task'
PLANNER_PARSER = 'planner'
REMINDER_PARSER = 'reminder'
DEPENDENCY_PARSER = 'dependency'


def setup_user_parser(user_parser):
    """
    Provide parser with arguments for users.

    user_parser -- ArgumentParser

    """

    user_parser.add_argument('command', help='add / show / change / rm')
    user_parser.add_argument('--id', help='Interacted user id.')
    user_parser.add_argument('--nickname', help='Unique username.')
    user_parser.add_argument('--email', help='User email.')
    user_parser.add_argument('--rate', help='User rating', type=int)
    user_parser.add_argument('--all', help='Show all users.', action="store_true", default=None)


def setup_task_parser(task_parser):
    """
    Provide parser with arguments for tasks.

    task_parser -- ArgumentParser

    """

    task_parser.add_argument('command', help='add / show / change / rm / move / attach / detach / groups / access')
    task_parser.add_argument('--id', help='Task id.', type=int)
    task_parser.add_argument('--name', help='Task name.')
    task_parser.add_argument('--content', help='Task description.')
    task_parser.add_argument('--deadline', help='Task deadline. Format: 23.10.2020 13:20')
    task_parser.add_argument('--status', help='Task status (Created - In Progress - Done / Failed).')
    task_parser.add_argument('--priority', help='Task priority.')
    task_parser.add_argument('--root', help='Root task id. Use it to show tree and add subtasks.', type=int)
    task_parser.add_argument('--tree', help='Display the tree of subtasks with the root --root.',
                             action="store_true", default=None)
    task_parser.add_argument('--all', help='Show all tasks.', action="store_true", default=None)
    task_parser.add_argument('--group', help='Group name.')
    task_parser.add_argument('--out', help='Remove from the group.', action="store_true", default=None)
    task_parser.add_argument('--member', help='Another user id (to manage access).', type=int)
    task_parser.add_argument('--level', help='Access level (can_read / can_confirm / can_edit).')
    task_parser.add_argument('--expire', help='Remove access to your task.', action="store_true", default=None)


def setup_planner_parser(planner_parser):
    """
    Provide parser with arguments for planners.

    planner_parser -- ArgumentParser

    """

    planner_parser.add_argument('command', help='add / show / change / rm')
    planner_parser.add_argument('--id', help='Planner id.', type=int)
    planner_parser.add_argument('--name', help='Generated task name.')
    planner_parser.add_argument('--content', help='Generated task description.')
    planner_parser.add_argument('--deadline', help='Generated task deadline.')
    planner_parser.add_argument('--priority', help='Generated task priority.')
    planner_parser.add_argument('--planning', help='Planning format like "13.05.0218 13:20 +5 days".')
    planner_parser.add_argument('--all', help='Show all planners.', action="store_true", default=None)


def setup_reminder_parser(reminder_parser):
    """
    Provide parser with arguments for reminders.

    reminder_parser -- ArgumentParser

    """

    reminder_parser.add_argument('command', help='add / show / change / rm')
    reminder_parser.add_argument('--id', help='Reminder id.', type=int)
    reminder_parser.add_argument('--text', help='Reminder notification text.')
    reminder_parser.add_argument('--time', help='Invoke time for non-regular reminder.')
    reminder_parser.add_argument('--planning', help='Planning format like "13.05.0218 13:20 +5 days".')
    reminder_parser.add_argument('--all', help='Show all reminders.', action="store_true", default=None)


def setup_dependency_parser(dep_parser):
    """
    Provide parser with arguments for
    dependant tasks and dependencies.

    dep_parser -- ArgumentParser

    """

    dep_parser.add_argument('command', help='add / show / rm')
    dep_parser.add_argument('--id', help='Dependant task id (show / rm)', type=int)
    dep_parser.add_argument('--first', help='Dependant task id (add)', type=int)
    dep_parser.add_argument('--second', help='ID of the main task.', type=int)
    dep_parser.add_argument('--behaviour', help='Dependency type (Direct / Inverse / Parallel).')


def setup_parser(parser):
    """
    Add all necessary arguments to our parser.

    Add subparsers to our parser. All necessary
    subparser arguments are provided by the
    corresponding methods above.

    parser -- ArgumentParser

    """

    parser.add_argument('--user', help='Username to use. Must exist in the database.')
    parser.add_argument('--config', help='Config file to use. Can be also put in $HOME/finetask.')

    subparsers = parser.add_subparsers(dest=ARGS_ENTITY)

    user_parser = subparsers.add_parser(USER_PARSER)
    setup_user_parser(user_parser)

    task_parser = subparsers.add_parser(TASK_PARSER)
    setup_task_parser(task_parser)

    planner_parser = subparsers.add_parser(PLANNER_PARSER)
    setup_planner_parser(planner_parser)

    reminder_parser = subparsers.add_parser(REMINDER_PARSER)
    setup_reminder_parser(reminder_parser)

    dep_parser = subparsers.add_parser(DEPENDENCY_PARSER)
    setup_dependency_parser(dep_parser)
