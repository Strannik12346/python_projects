from argparse import ArgumentParser

from finetask.console.parser import setup_parser
from finetask.console.configuration import (
    set_config_with_user,
    set_config_from_file,
    set_config_from_default_location,
    set_default_config
)
from finetask.console.representation import Representation
from finetask.console.logging import log_args
from finetask.library.logging import get_logger


class Application:
    def __init__(self, args):
        self.args = args
        self.interface = Representation(args.database)

        self.commands = {
            ('user', 'show'): self.interface.show_user,
            ('user', 'add'): self.interface.add_user,
            ('user', 'change'): self.interface.change_user,
            ('user', 'rm'): self.interface.remove_user,

            ('task', 'show'): self.interface.show_task,
            ('task', 'add'): self.interface.add_task,
            ('task', 'change'): self.interface.change_task,
            ('task', 'rm'): self.interface.remove_task,
            ('task', 'move'): self.interface.move_task,
            ('task', 'attach'): self.interface.attach_task,
            ('task', 'detach'): self.interface.detach_task,
            ('task', 'groups'): self.interface.show_task_groups,
            ('task', 'access'): self.interface.manage_permissions,

            ('planner', 'show'): self.interface.show_planner,
            ('planner', 'add'): self.interface.add_planner,
            ('planner', 'change'): self.interface.change_planner,
            ('planner', 'rm'): self.interface.remove_planner,

            ('reminder', 'show'): self.interface.show_reminder,
            ('reminder', 'add'): self.interface.add_reminder,
            ('reminder', 'change'): self.interface.change_reminder,
            ('reminder', 'rm'): self.interface.remove_reminder,

            ('dependency', 'show'): self.interface.show_dependency,
            ('dependency', 'add'): self.interface.add_dependency,
            ('dependency', 'rm'): self.interface.remove_dependency
        }

    def start(self):
        if not self.interface.validate_all(self.args):
            logger = get_logger()
            logger.error('Validation failed due to args format.')
            return

        self.interface.remind_all(self.args)
        self.interface.fail_all_deadlines(self.args)

        try:
            key = (self.args.entity, self.args.command)
            funct = self.commands[key]
            funct(self.args)
        except Exception as ex:
            print('An error occured. The command is not supported.')
            print((self.args.entity, self.args.command))
            logger = get_logger()
            logger.error(ex)


def main():
    parser = ArgumentParser(description='Use FineTask with the command you specify.', prog='finetask')

    setup_parser(parser)
    args = parser.parse_args()

    config_already_set = False

    if args.user is not None and not config_already_set:
        config_already_set = set_config_with_user(args, args.user)

    if args.config is not None and not config_already_set:
        config_already_set = set_config_from_file(args, args.config)

    if not config_already_set:
        config_already_set = set_config_from_default_location(args)

    if not config_already_set:
        config_already_set = set_default_config(args)
        print('Config was set to default.')

    assert config_already_set

    log_args(args)  # Logging all the arguments on INFO level

    try:
        app = Application(args)
        app.start()
    except Exception as ex:
        logger = get_logger()
        logger.error(ex)


if __name__ == '__main__':
    main()
