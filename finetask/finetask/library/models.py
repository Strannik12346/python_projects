STATUS_CREATED = 'Created'
STATUS_IN_PROGRESS = 'In Progress'
STATUS_DONE = 'Done'
STATUS_FAILED = 'Failed'

PRIORITY_HIGH = 'High'
PRIORITY_MEDIUM = 'Medium'
PRIORITY_LOW = 'Low'

BEHAVIOUR_DIRECT = 'Direct'
BEHAVIOUR_INVERSE = 'Inverse'
BEHAVIOUR_PARALLEL = 'Parallel'


class User:
    def __init__(self, nickname, email, rate):
        self.nickname = nickname
        self.email = email
        self.rate = rate

    def __str__(self):
        return '| ID:{id}\t| {nick} | email: {email} | rate: {rate}'.format(id=self.id,
                                                                            nick=self.nickname,
                                                                            email=self.email,
                                                                            rate=self.rate)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return (self.nickname == other.nickname and
                self.email == other.email and
                self.rate == other.rate)

    def short_str(self):
        return '[ID:{id}] {nick} with rate {rate}'.format(id = self.id, nick=self.nickname, rate=self.rate)


class Task:
    def __init__(self, name, content, status, time_created, time_deadline,
                 priority, owner_id, planner_id=None, parent_id=None):
        self.name = name
        self.content = content
        self.status = status
        self.time_created = time_created
        self.time_deadline = time_deadline
        self.priority = priority
        self.owner_id = owner_id
        self.planner_id = planner_id
        self.parent_id = parent_id

    INT_TO_IMPORTANCE_LEVEL = {
        0: PRIORITY_LOW,
        10: PRIORITY_MEDIUM,
        20: PRIORITY_HIGH
    }

    IMPORTANCE_LEVEL_TO_INT = {
        PRIORITY_LOW: 0,
        PRIORITY_MEDIUM: 10,
        PRIORITY_HIGH: 20
    }

    @classmethod
    def get_string_priority(cls, int_priority):
        assert isinstance(int_priority, int)

        str_priority = cls.INT_TO_IMPORTANCE_LEVEL.get(int_priority)
        if str_priority is None:
            str_priority = str(int_priority)

        return str_priority

    @classmethod
    def get_int_priority(cls, str_priority):
        assert isinstance(str_priority, str)

        int_priority = cls.IMPORTANCE_LEVEL_TO_INT.get(str_priority)
        if int_priority is None:
            int_priority = int(str_priority)

        return int_priority

    def __str__(self):
        priority = Task.get_string_priority(self.priority)
        return '[priority:{prior}] [ID:{id}] "{name}" with status: "{status}";'.format(id=self.id,
                                                                                       name=self.name,
                                                                                       status=self.status,
                                                                                       prior=priority)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return (self.name == other.name and
                self.content == other.content and
                self.status == other.status and
                self.priority == other.priority and
                self.owner_id == other.owner_id and
                self.planner_id == other.planner_id and
                self.parent_id == other.parent_id)

    def to_str_list(self):
        """Return list of strings representing a task."""

        return ['Name: ' + self.name,
                'Content: ' + self.content,
                'Status: ' + self.status,
                'Created: ' + self.time_created,
                'Deadline: ' + self.time_deadline,
                'Priority: ' + self.get_string_priority(self.priority)]

    def is_ended(self):
        return self.status == STATUS_DONE or self.status == STATUS_FAILED


class Planner:
    def __init__(self, task_name, task_content, task_deadline, task_priority, shift, shift_type, owner_id):
        self.task_name = task_name
        self.task_content = task_content
        self.task_deadline = task_deadline
        self.task_priority = task_priority
        self.shift = shift
        self.shift_type = shift_type
        self.owner_id = owner_id

    def __str__(self):
        return '| ID:{id}\t| {name}" - once in {count} {unit}; content: {cont}; priority: {prior};'.format(
            id=self.id,
            name=self.task_name,
            count=self.shift,
            unit=self.shift_type,
            cont=self.task_content,
            prior=Task.get_string_priority(self.task_priority)
        )

    def __repr__(self):
        return str(self)

    def short_str(self):
        """More short form of str representation"""

        return '[ID:{id}] - makes task once in {count} {unit}'.format(id=self.id,
                                                                      count=self.shift,
                                                                      unit=self.shift_type)


class Reminder:
    def __init__(self, text, invoke_time, owner_id, shift=None, shift_type=None):
        self.text = text
        self.invoke_time = invoke_time
        self.shift = shift
        self.shift_type = shift_type
        self.owner_id = owner_id

    def __str__(self):
        if self.shift is not None:
            regular_str = ' - occurs once in {count} {unit}'.format(count=self.shift, unit=self.shift_type)
        else:
            regular_str = ''

        return '| ID:{id}\t| {time} | "{text}"'.format(id=self.id, time=self.invoke_time, text=self.text) + regular_str

    def __repr__(self):
        return str(self)

    def is_regular(self):
        return self.shift is not None and self.shift_type is not None


class Dependency:
    def __init__(self, first_id, second_id, behaviour, owner_id):
        self.taskA_id = first_id
        self.taskB_id = second_id
        self.behaviour = behaviour
        self.owner_id = owner_id

    def __str__(self):
        return '| ID:{id}\t| {dep_type} dependency : "task #{A}" -->> "task #{B}"'.format(id=self.id,
                                                                                          dep_type=self.behaviour,
                                                                                          A=self.taskA_id,
                                                                                          B=self.taskB_id)

    def __repr__(self):
        return str(self)

    def short_str(self):
        """More short form of str representation"""

        return 'task [ID:{id}] with {dep_type} behaviour '.format(id=self.taskB_id, dep_type=self.behaviour)

    @classmethod
    def allows_to_set(cls, main_status, behaviour, new_status):
        """
        Return if the dependency allows to set new_status.

        main_status -- 'Created', 'In Progress', 'Done' or 'Failed', it
                   refers to the main, NOT dependant task,
        behaviour -- 'Direct', 'Inverse' or 'Parallel',
        new_status -- the same as status for dependant task.

        """

        if behaviour == BEHAVIOUR_DIRECT:
            if new_status != STATUS_DONE:
                return True
            else:
                return main_status == STATUS_DONE

        elif behaviour == BEHAVIOUR_INVERSE:
            if new_status != STATUS_DONE:
                return True
            else:
                return main_status == STATUS_FAILED

        elif behaviour == BEHAVIOUR_PARALLEL:
            if new_status == STATUS_CREATED:
                return main_status == STATUS_CREATED or main_status == STATUS_IN_PROGRESS
            elif new_status == STATUS_IN_PROGRESS or new_status == STATUS_FAILED:
                return True
            elif new_status == STATUS_DONE:
                return main_status == STATUS_IN_PROGRESS or main_status == STATUS_DONE


class GroupRecord:
    def __init__(self, group_name, task_id, owner_id):
        self.group_name = group_name
        self.task_id = task_id
        self.owner_id = owner_id

    def __str__(self):
        return self.group_name

    def __repr__(self):
        return str(self)


class Permission:
    def __init__(self, owner_id, guest_id, task_id, level):
        self.owner_id = owner_id
        self.guest_id = guest_id
        self.task_id = task_id
        self.level = level

    def __str__(self):
        return 'Permissions for the user #{id} with level [{level}] (task #{task});'.format(id=self.guest_id,
                                                                                            level=self.level,
                                                                                            task=self.task_id)

    def __repr__(self):
        return str(self)
