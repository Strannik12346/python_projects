from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import clear_mappers
from sqlalchemy.orm import mapper

from finetask.library.models import (
    User,
    Task,
    Planner,
    Reminder,
    Dependency,
    GroupRecord,
    Permission
)


PERMISSION_OWNER = 'owner'
PERMISSION_RESTRICTED = 'restricted'


class Storage:
    def __init__(self, filename):
        """Define the file with our database."""

        self.filename = filename
        self.engine = create_engine('sqlite:///' + self.filename)

        self._create_tables()
        self._create_mappers()

        self.Session = sessionmaker(bind=self.engine)

    def _create_tables(self):
        """
        Create tables with the structure below:

        'users' (id, nickname, email, rate)

        'tasks' (id, name, content, status,
                 time_created, time_deadline, priority,
                 owner_id, planner_id, parent_id)

        'planners' (id, task_name, task_content, task_deadline,
                    task_priority. shift, shift_type, owner_id)

        'reminders' (id, text, invoke_time, shift, shift_type, owner_id)

        'dependencies' (id, first_task, second_task, behaviour)

        """

        metadata = MetaData()

        self.users_table = Table('users', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('nickname', String, nullable=False, unique=True),
            Column('email', String, nullable=False),
            Column('rate', Integer, nullable=False)
        )

        self.tasks_table = Table('tasks', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('name', String, nullable=False),
            Column('content', String, nullable=False),
            Column('status', String, nullable=False),
            Column('time_created', String, nullable=False),
            Column('time_deadline', String, nullable=False),
            Column('priority', Integer, nullable=False),
            Column('owner_id', Integer, ForeignKey('users.id'), nullable=False),
            Column('planner_id', Integer, ForeignKey('planners.id'), nullable=True),
            Column('parent_id', Integer, ForeignKey('tasks.id'), nullable=True)
        )

        self.planners_table = Table('planners', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('task_name', String, nullable=False),
            Column('task_content', String, nullable=False),
            Column('task_deadline', String, nullable=False),
            Column('task_priority', Integer, nullable=False),
            Column('shift', Integer, nullable=False),
            Column('shift_type', String, nullable=False),
            Column('owner_id', Integer, nullable=False)
        )

        self.reminders_table = Table('reminders', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('text', String, nullable=False),
            Column('invoke_time', String, nullable=False),
            Column('shift', Integer, nullable=True),
            Column('shift_type', String, nullable=True),
            Column('owner_id', Integer, ForeignKey('users.id'), nullable=False)
        )

        self.deps_table = Table('dependencies', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('taskA_id', Integer, ForeignKey('tasks.id'), nullable=False),
            Column('taskB_id', Integer, ForeignKey('tasks.id'), nullable=False),
            Column('behaviour', String, nullable=False),
            Column('owner_id', Integer, ForeignKey('users.id'), nullable=False)
        )

        self.groups_table = Table('groups', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('group_name', String, nullable=False),
            Column('task_id', Integer, ForeignKey('tasks.id'), nullable=False),
            Column('owner_id', Integer, ForeignKey('users.id'), nullable=False),
        )

        self.rights_table = Table('access_rights', metadata,
            Column('id', Integer, nullable=False, primary_key=True, autoincrement=True),
            Column('owner_id', Integer, ForeignKey('users.id'), nullable=False),
            Column('guest_id', Integer, ForeignKey('users.id'), nullable=False),
            Column('task_id', Integer, ForeignKey('tasks.id'), nullable=False),
            Column('level', String, nullable=False)
        )

        metadata.create_all(self.engine)

    def _create_mappers(self):
        clear_mappers()
        mapper(User, self.users_table)
        mapper(Task, self.tasks_table)
        mapper(Planner, self.planners_table)
        mapper(Reminder, self.reminders_table)
        mapper(Dependency, self.deps_table)
        mapper(GroupRecord, self.groups_table)
        mapper(Permission, self.rights_table)

    def extract_table_names(self):
        """Return all table name that we have in the form list<str>."""

        return [self.users_table.name, self.tasks_table.name, self.planners_table.name,
                self.reminders_table.name, self.deps_table.name, self.groups_table.name]

    # REGION USERS

    def insert_user(self, name, email, rate):
        """
        Add new user to the database. Return user.

        Throws exception if such nickname exists.

        name -- str,
        email -- str,
        rate -- int.

        """

        session = self.Session()
        user = User(name, email, rate)
        session.add(user)
        session.commit()
        return user

    def extract_user(self, user_id):
        """Get user by ID and return models.User. If user does not exist, return None."""

        session = self.Session()
        user = session.query(User).filter_by(id=user_id).first()
        return user

    def find_user(self, name):
        """Find user by his name. Return models.User. If user doesn't exist, return None."""

        session = self.Session()
        user = session.query(User).filter_by(nickname=name).first()
        return user

    def find_all_users(self):
        """Extract all users from the database. Return list<User>."""

        session = self.Session()
        users = session.query(User).all()
        return users

    def update_user(self, user_id, new_name=None, new_email=None, new_rate=None):
        """
        Update user with id=user_id with new data. Return user.

        Throws exception if such name already exists.

        user_id -- id of the affected user,
        new_name -- str, only if you want to change it,
        new_email -- str, only if you want to change it,
        new_rate -- str, only if you want to change it.

        """

        session = self.Session()
        user = session.query(User).filter_by(id=user_id).first()
        if new_name is not None:
            user.nickname = new_name
        if new_email is not None:
            user.email = new_email
        if new_rate is not None:
            user.rate = new_rate
        session.commit()

        return user

    def remove_user(self, user_id):
        """Remove user by his ID. Also remove all owned by him."""

        session = self.Session()

        session.query(User).filter_by(id=user_id).delete()
        session.query(Task).filter_by(owner_id=user_id).delete()
        session.query(Planner).filter_by(owner_id=user_id).delete()
        session.query(Reminder).filter_by(owner_id=user_id).delete()
        session.query(Dependency).filter_by(owner_id=user_id).delete()
        session.query(GroupRecord).filter_by(owner_id=user_id).delete()
        session.query(Permission).filter_by(owner_id=user_id).delete()

        session.commit()

    # REGION TASKS

    def insert_task(self, name, content, status, time_created, time_deadline,
                    priority, owner_id, planner_id, parent_id):
        """
        Add new task to the database. Return models.Task.

        name -- str,
        content -- str,
        status -- "Created", "In Progress", "Done" or "Failed",
        time_created -- str in format: "22.02.2010 23:57",
        time_deadline - str in the same format,
        priority -- int > 0,
        owner_id -- int > 0 (link to owner),
        planner_id -- int > 0 or None (link to planner),
        parent_id -- int > 0 or None (link to the task-parent).

        """

        session = self.Session()
        task = Task(name, content, status, time_created, time_deadline, priority, owner_id, planner_id, parent_id)
        session.add(task)
        session.commit()
        return task

    def extract_task(self, task_id):
        """Get task by its ID. Return models.Task. If task doesn't exist, return None."""

        session = self.Session()
        task = session.query(Task).filter_by(id=task_id).first()
        return task

    def find_tasks(self, task_name):
        """Get tasks with the matching name. Return list<Task>."""

        session = self.Session()
        tasks = session.query(Task).filter_by(name=task_name).all()
        return tasks

    def find_all_tasks(self, owner_id):
        """Get all tasks for current user. Return list<Task>"""

        session = self.Session()
        tasks = session.query(Task).filter_by(owner_id=owner_id).all()
        return tasks

    def find_subtasks(self, root_id):
        """Get all the subtask of the root task. Return list<Task>"""

        session = self.Session()
        tasks = session.query(Task).filter_by(parent_id=root_id).all()
        return tasks

    def find_tasks_by_status(self, owner_id, status):
        """Get all the tasks for this user with this status. Return list<Task>."""

        session = self.Session()
        tasks = session.query(Task).filter_by(owner_id=owner_id, status=status).all()
        return tasks

    def update_task(self, task_id, new_name=None, new_content=None, new_status=None, new_time_created=None,
                    new_time_deadline=None, new_priority=None, new_owner_id=-1, new_planner_id=-1, new_parent_id=-1):
        """
        Update task with id=task_id with new data.

        new_name -- str,
        new_content -- str,
        new_status -- "Created", "In Progress", "Done" or "Failed",
        new_time_created -- str in format: "22.02.2010 23:57",
        new_time_deadline - str in the same format,
        new_priority -- int > 0,
        new_owner_id -- int > 0 (link to owner),
        new_planner_id -- int > 0 or None (link to planner),
        new_parent_id -- int > 0 or None (link to the task-parent).

        """

        session = self.Session()
        task = session.query(Task).filter_by(id=task_id).first()

        if new_name is not None:
            task.name = new_name

        if new_content is not None:
            task.content = new_content

        if new_status is not None:
            task.status = new_status

        if new_time_created is not None:
            task.time_created = new_time_created

        if new_time_deadline is not None:
            task.time_deadline = new_time_deadline

        if new_priority is not None:
            task.priority = new_priority

        if new_owner_id != -1:
            task.owner_id = new_owner_id

        if new_planner_id != -1:
            task.planner_id = new_planner_id

        if new_parent_id != -1:
            task.parent_id = new_parent_id

        session.commit()

    def remove_task(self, task_id):
        """Remove task with this id and all dependencies for it."""

        session = self.Session()

        session.query(Task).filter_by(id=task_id).delete()
        session.query(Dependency).filter_by(taskA_id=task_id).delete()
        session.query(Dependency).filter_by(taskB_id=task_id).delete()

        session.commit()

    # REGION PLANNERS

    def insert_planner(self, task_name, task_content, task_deadline, task_priority, shift, shift_type, owner_id):
        """
        Add new planner to the database. Return its object.

        task_name -- str,
        task_content -- str
        task_deadline -- str in format: "22.02.2010 23:57",
        shift -- int > 0 or None,
        shift_type -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week', also may be None,
        owner_id -- int > 0 (link to owner).

        """

        session = self.Session()
        planner = Planner(task_name, task_content, task_deadline, task_priority, shift, shift_type, owner_id)
        session.add(planner)
        session.commit()
        return planner

    def extract_planner(self, planner_id):
        """Get the planner with this id and return models.Planner. If the planner doesn't exist, return None."""

        session = self.Session()
        planner = session.query(Planner).filter_by(id=planner_id).first()
        return planner

    def update_planner(self, planner_id, new_name=None, new_content=None, new_priority=None, new_deadline=None):
        """
        Change some planner features.

        new_name -- str
        new_content -- str
        new_priority -- int > 0
        new_deadline -- str in the format: '13.09.2017 13:02'

        """

        session = self.Session()
        planner = session.query(Planner).filter_by(id=planner_id).first()

        if new_name is not None:
            planner.task_name = new_name
        if new_content is not None:
            planner.task_content = new_content
        if new_priority is not None:
            planner.task_priority = new_priority
        if new_deadline is not None:
            planner.task_deadline = new_deadline

        session.commit()

    def find_planners(self, task_name):
        """Get planners, which generate tasks with this name. Return list<Planner>."""

        session = self.Session()
        planners = session.query(Planner).filter_by(task_name=task_name).all()
        return planners

    def find_all_planners(self, owner_id):
        """Get all planners for current user. Return list<Planner>."""

        session = self.Session()
        planners = session.query(Planner).filter_by(owner_id=owner_id).all()
        return planners

    def remove_planner(self, planner_id):
        """Remove planner with this id."""

        session = self.Session()
        session.query(Planner).filter_by(id=planner_id).delete()
        session.commit()

    # REGION REMINDERS

    def insert_reminder(self, text, invoke_time, shift, shift_type, owner_id):
        """
        Add new reminder to the database. Return models.Reminder.

        text -- str,
        invoke_time -- str in format: "22.02.2010 23:57",
        shift -- int > 0,
        shift_type -- 'hours' or 'hour', 'days' or 'day', 'weeks' or 'week',
        owner_id -- int > 0 (link to owner).

        """

        session = self.Session()
        reminder = Reminder(text, invoke_time, owner_id, shift, shift_type)
        session.add(reminder)
        session.commit()
        return reminder

    def extract_reminder(self, reminder_id):
        """Get reminder by its id and return Reminder. If the reminder doesn't exist, return None."""

        session = self.Session()
        reminder = session.query(Reminder).filter_by(id=reminder_id).first()
        return reminder

    def find_reminders(self, text):
        """Get reminders by matching name. Return list<Reminder>."""

        session = self.Session()
        reminders = session.query(Reminder).filter_by(text=text).all()
        return reminders

    def find_all_reminders(self, owner_id):
        """Get all the reminders of this user. Return list<Reminder>."""

        session = self.Session()
        reminders = session.query(Reminder).filter_by(owner_id=owner_id).all()
        return reminders

    def update_reminder(self, reminder_id, new_text=None, new_invoke_time=None):
        """
        Update reminder invoke time with new values.

        reminder_id -- int > 0
        new_text -- str
        new_invoke_time -- str in the format '12.09.2018 13:00'

        """

        session = self.Session()
        reminder = session.query(Reminder).filter_by(id=reminder_id).first()

        if new_text is not None:
            reminder.text = new_text
        if new_invoke_time is not None:
            reminder.invoke_time = new_invoke_time

        session.commit()

    def remove_reminder(self, reminder_id):
        """Remove reminder with this id."""

        session = self.Session()
        session.query(Reminder).filter_by(id=reminder_id).delete()
        session.commit()

    # REGION DEPENDENCIES

    def insert_dependency(self, first_id, second_id, behaviour, owner_id):
        """
        Make task dependant.

        Set dependency for task with id=first_id. Task with id=second_id
        will be main task. Return Dependency object.
        behaviour defines the effect of the dependency.

        first_id -- int > 0 (link to the first task),
        second_id -- int > 0 (link to the second task),
        behaviour -- 'Direct', 'Inverse' or 'Parallel'.
        owner_id -- int > 0

        """

        session = self.Session()
        dep = Dependency(first_id, second_id, behaviour, owner_id)
        session.add(dep)
        session.commit()
        return dep

    def extract_dependency(self, dep_task_id):
        """Get the dependency using dependant task id. Return models.Dependency."""

        session = self.Session()
        dep = session.query(Dependency).filter_by(taskA_id=dep_task_id).first()
        return dep

    def remove_dependency(self, dep_task_id):
        """Remove dependency of the task with id=dep_task_id."""

        session = self.Session()
        session.query(Dependency).filter_by(taskA_id=dep_task_id).delete()
        session.commit()

    # REGION groups

    def add_to_group(self, group_name, task_id, user_id):
        """
        Add task to group for this user.

        It doesn't matter, if group already exists or not.

        group_name -- str
        task_id -- int > 0
        user_id -- int > 0

        """

        session = self.Session()
        grp = GroupRecord(group_name, task_id, user_id)
        session.add(grp)
        session.commit()

    def remove_from_group(self, user_id, group_name, task_id):
        """Remove task from group for current user."""

        session = self.Session()
        session.query(GroupRecord).filter_by(owner_id=user_id, group_name=group_name, task_id=task_id).delete()
        session.commit()

    def extract_group(self, user_id, group_name):
        """Get all tasks from this group of this user. Return list<Task>"""

        session = self.Session()
        tasks = session.query(Task).join(GroupRecord).filter_by(owner_id=user_id, group_name=group_name).all()
        return tasks

    def extract_group_names(self, user_id):
        """Get all group names for this user. Return sorted list."""

        session = self.Session()
        records = session.query(GroupRecord).filter_by(owner_id=user_id).all()

        group_names = [rec.group_name for rec in records]
        return sorted(set(group_names))

    def extract_all_groups(self, user_id):
        """
        Get all tasks of this user.

        Return dict, where
        key is group name and
        element is list<Task>.

        """

        response = dict()
        for name in self.extract_group_names(user_id):
            response[name] = self.extract_group(user_id, name)

        return response

    # REGION permissions

    def extract_permission(self, guest_id, task_id):
        """Return Permission object for this user as guest and this task."""

        session = self.Session()
        return session.query(Permission).filter_by(guest_id=guest_id, task_id=task_id).first()

    def find_all_granted_permissions(self, task_id):
        """Return list<Permission> for this owner and this task."""

        session = self.Session()
        return session.query(Permission).filter_by(task_id=task_id).all()

    def insert_permission(self, owner_id, guest_id, task_id, level):
        """Make new permission for this task. If it already exists, replace it."""

        if self.extract_permission(guest_id, task_id) is not None:
            self.remove_permission(guest_id, task_id)

        session = self.Session()
        new_permission = Permission(owner_id, guest_id, task_id, level)
        session.add(new_permission)
        session.commit()

    def remove_permission(self, guest_id, task_id):
        """Remove permision for this task granted to this user."""

        session = self.Session()
        session.query(Permission).filter_by(guest_id=guest_id, task_id=task_id).delete()
        session.commit()

    def check_permissions(self, user_id, task_id):
        """
        Check permissions for this user. Return string.

        Check if this user is the owner of the task or
        he can read/confirm/edit this task. Return one of the following strings:
        'restricted', 'can_read', 'can_confirm', 'can_edit', 'owner'.

        """

        task = self.extract_task(task_id)
        if task is None:
            return None

        if task.owner_id == user_id:
            return PERMISSION_OWNER

        permission = self.extract_permission(user_id, task_id)
        if permission is None:
            return PERMISSION_RESTRICTED

        return permission.level
