MSG_USER_EXISTS = 'Such a name exists!'
MSG_DEPENDENCY_EXISTS = 'Task is already dependant.'
MSG_PERMISSION_DENIED = 'You have no rights to do this!'
MSG_CYCLE_DETECTED = 'The action you demand creates cycle!'
MSG_MOVE_TO_STANDARD_GROUP = 'Cannot move to standard group.'
MSG_IS_ALREADY_OWNER = 'You are trying to give access to yourselves!'

MSG_NO_USER = 'No user found.'
MSG_NO_TASK = 'No task found.'
MSG_NO_PLANNER = 'No planner found.'
MSG_NO_REMINDER = 'No reminder found.'
MSG_NO_DEPENDENCY = 'No dependency found.'
MSG_NO_PERMISSION = 'No permission found.'


class InvalidActionError(Exception):
    """Base class for exceptions in our library."""

    def __init__(self, message):
        self.message = message


class ExistingUserError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_USER_EXISTS)


class ExistingDependencyError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_DEPENDENCY_EXISTS)


class PermissionDeniedError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_PERMISSION_DENIED)


class TaskTreeCycleError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_CYCLE_DETECTED)


class InvalidGroupMoveError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_MOVE_TO_STANDARD_GROUP)


class StatusNotAllowedError(InvalidActionError):
    def __init__(self, task_id, status):
        message = 'Cannot set status "{stat}" to the task #{id}'.format(stat=status, id=task_id)
        super().__init__(message)


class InvalidAttachError(InvalidActionError):
    def __init__(self, root_id):
        message = 'Root task #{root_id} does not exist!'.format(root_id=root_id)
        super().__init__(message)


class InvalidTaskDeleteError(InvalidActionError):
    def __init__(self, task_id):
        message = 'Task #{id} cannot be deleted because of subtasks.'.format(id=task_id)
        super().__init__(message)


class NotNecessaryPermissionError(InvalidActionError):
    def __init__(self):
        super().__init__(MSG_IS_ALREADY_OWNER)


class NoEntityError(InvalidActionError):
    """
    Base class for exceptions that mean "no entity".

    Descendants:

    NoUserError
    NoPlannerError
    NoReminderError
    NoDependencyError
    NoPermissionError

    """

    def __init(self, message):
        super().__init__(message)


class NoUserError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_USER)


class NoTaskError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_TASK)


class NoPlannerError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_PLANNER)


class NoReminderError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_REMINDER)


class NoDependencyError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_DEPENDENCY)


class NoPermissionError(NoEntityError):
    def __init__(self):
        super().__init__(MSG_NO_PERMISSION)
