import subprocess
import unittest
import time
import json
import os

from finetask.library.interaction import TEST_USERNAME
from finetask.tests.get_tests_console import get_test_data


APPLICATION_NAME = 'finetask'
TEST_DIRECTORY = 'tests'
TEST_DATABASE = 'finetask_test_database.db'
LOGGING_FILE = 'finetask.log'

USER_JSON = 'user.json'
LOG_LEVEL = 'info'


class ExternalTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        current_dir = os.getcwd()
        test_directory = os.path.join(current_dir, TEST_DIRECTORY)

        cls.user_file = os.path.join(test_directory, USER_JSON)
        cls.log_file = os.path.join(test_directory, LOGGING_FILE)
        cls.test_database = os.path.join(test_directory, TEST_DATABASE)  # To delete correctly

        if os.path.exists(cls.test_database):
            os.remove(cls.test_database)

        user_data = json.dumps(
            {
                'database': cls.test_database,
                'log': cls.log_file,
                'logging': LOG_LEVEL,
                'name': TEST_USERNAME
            },
            sort_keys=True, indent=4
        )

        user_data += '\n'

        if not os.path.exists(test_directory):
            os.mkdir(test_directory)

        with open(cls.user_file, 'w') as file:
            file.write(user_data)
        print(user_data)

    @classmethod
    def print_finetask_output(cls, finetask_args_list):
        config_argument = '--config=' + cls.user_file
        args = [APPLICATION_NAME, config_argument] + finetask_args_list
        print(subprocess.check_output(args, universal_newlines=True))

    @classmethod
    def return_finetask_output(cls, finetask_args_list):
        config_argument = '--config=' + cls.user_file
        args = [APPLICATION_NAME, config_argument] + finetask_args_list
        return subprocess.check_output(args, universal_newlines=True)

    @classmethod
    def split_and_count_lines(cls, str_output):
        output_lines = str_output.splitlines()

        result = len(output_lines)
        for el in output_lines:
            if len(el) < 3:
                result -= 1

        return (result, output_lines)

    @classmethod
    def has_all_substrings(cls, text, substrings):
        text = text.upper()
        result = True

        for substring in substrings:
            substring = substring.upper()
            result = result and substring in text
        return result

    def test_all(self):
        i = 1
        for test in get_test_data():
            command, required_lines_count, substrings = test

            response = self.return_finetask_output(command)
            lines_count, output_lines = self.split_and_count_lines(response)

            print('<Test{num}>'.format(num=i))
            for el in output_lines:
                print(el)
            print('</Test{num}>\n'.format(num=i))

            self.assertEqual(lines_count, required_lines_count)
            self.assertTrue(self.has_all_substrings(response, substrings))

            i += 1

        time.sleep(0.3)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.user_file):
            os.remove(cls.user_file)
        if os.path.exists(cls.test_database):
            os.remove(cls.test_database)


if __name__ == '__main__':
    unittest.main()
