from datetime import datetime
from datetime import timedelta

from finetask.library.interaction import DATETIME_FORMAT


def get_test_data():
    """
    Generate a set of 45 tests. Return array of tuples.

    Generates array of 45 tuples, where each tuple in format:
    ([list of args], [lines_count], [list of substrings]).

    Generated values are used to parametrize tests in py.test.

    """

    half = timedelta(minutes=30)
    now = datetime.now()
    near = (now + half).strftime(DATETIME_FORMAT)
    future = '23.02.2019 13:20'

    # all_tests -- array of tuples
    #
    # Each tuple represents a test.
    # Tuple format: ([list of args], [lines_count],
    # [list of substrings]),
    #
    # During tests execution,
    # Each response will be matched by lines count
    # and by key substrings.
    #
    # Using pytest and parametrize() decorator
    # there must be 45 parametrized tests.

    all_tests = [
        (['user', 'show', '--all'], 7,
         ['root', 'test']),

        (['reminder', 'add', '--text=Remind me', '--time=' + near], 4,
         ['reminder', '#1']),

        (['user', 'add', '--nickname=username', '--email=first@mail.ru', '--rate=0'], 5,
         ['user', '#3']),

        (['user', 'show', '--all'], 9,
         ['root', 'test', 'username']),

        (['user', 'show', '--id=3'], 5,
         ['first@mail.ru']),

        (['user', 'change', '--rate=100'], 5,
         ['updated rate', '#2']),

        (['task', 'add', '--name=t1', '--content=cnt', '--priority=1', '--deadline=' + future], 5,
         ['task', '#1']),

        (['task', 'add', '--name=t2', '--content=cnt', '--priority=2', '--root=1', '--deadline=' + future], 5,
         ['task', '#2']),

        (['task', 'add', '--name=t3', '--content=cnt', '--priority=3', '--root=1', '--deadline=' + future], 5,
         ['task', '#3']),

        (['task', 'add', '--name=t4', '--content=cnt', '--priority=4', '--root=1', '--deadline=' + future], 5,
         ['task', '#4']),

        (['task', 'change', '--id=1', '--name=root'], 5,
         ['Successfully', 'name', '#1']),

        (['task', 'change', '--id=2', '--name=fastened1'], 5,
         ['Successfully', 'name', '#2']),

        (['task', 'change', '--id=3', '--name=fastened2'], 5,
         ['Successfully', 'name', '#3']),

        (['task', 'change', '--id=4', '--name=fastened3'], 5,
         ['Successfully', 'name', '#4']),

        (['task', 'show', '--all'], 10,
         ['[ID:1]', '[ID:2]', '[ID:3]', '[ID:4]']),

        (['task', 'show', '--root=1', '--tree'], 8,
         ['* [priority:', '[ID:1]', '[ID:2]', '[ID:3]', '[ID:4]']),

        (['task', 'show', '--id=1'], 15,
         ['Name', 'Content', 'Status', 'Access']),

        (['task', 'move', '--id=1', '--group=Family'], 5,
         ['#1', 'Family']),

        (['task', 'detach', '--id=3'], 5,
         ['detached', '#3']),

        (['task', 'attach', '--id=3', '--root=2'], 5,
         ['attached', '#3']),

        (['task', 'show', '--root=1', '--tree'], 8,
         ['root', '[ID:3]']),

        (['task', 'rm', '--id=3'], 5,
         ['#3', 'deleted']),

        (['task', 'show', '--root=1', '--tree'], 7,
         ['[ID:1]', '[ID:2]', '[ID:4]']),

        (['planner', 'add', '--name=PlanName', '--content=PlanContent', '--planning=23.02.2018 20:00 +5 days'], 5,
         ['planner', '#1']),

        (['planner', 'show', '--id=1'], 5,
         ['5 days']),

        (['planner', 'change', '--id=1', '--name=NEW', '--content=NEWplannerCONTENT'], 6,
         ['planner', '#1', 'name', 'content']),

        (['planner', 'show', '--id=1'], 5,
         ['ID:1', 'NEW', 'NEWplannerCONTENT', '5 days']),

        (['planner', 'show', '--all'], 7,
         ['5 days']),

        (['task', 'show', '--all'], 11,
         ['']),

        (['planner', 'rm', '--id=1'], 5,
         ['#1', 'deleted']),

        (['planner', 'show', '--all'], 4,
         ['']),

        (['reminder', 'add', '--text=RemName', '--planning=23.12.2018 13:00 +5 days'], 5,
         ['reminder', '#2']),

        (['reminder', 'show', '--id=1'], 5,
         ['ID:1', 'Remind me']),

        (['reminder', 'show', '--all'], 8,
         ['ID:1', 'ID:2', '5 days']),

        (['reminder', 'change', '--id=2', '--text=NEWTEXT'], 5,
         ['reminder', '#2']),

        (['reminder', 'change', '--id=2', '--time=20.10.2020 13:20'], 5,
         ['reminder', '#2']),

        (['reminder', 'show', '--id=2'], 5,
         ['ID:2', 'NEWTEXT', '20.10.2020 13:20']),

        (['reminder', 'rm', '--id=2'], 5,
         ['#2', 'deleted']),

        (['dependency', 'add', '--first=1', '--second=2', '--behaviour=Direct'], 5,
         ['dependency', '#1']),

        (['dependency', 'show', '--id=1'], 5,
         ['#1', '#2', 'Direct']),

        (['dependency', 'rm', '--id=1'], 5,
         ['#1', 'no more']),

        (['reminder', 'rm', '--id=7788'], 5,
         ['No', 'reminder', 'found']),

        (['reminder', 'rm', '--id=1'], 5,
         ['reminder', 'delete', '#1']),

        (['reminder', 'show', '--id=1'], 4,
         ['No reminder found']),

        (['reminder', 'something', '--id=1'], 5,
         ['The', 'command', 'not', 'supported', 'reminder', 'something'])
    ]

    return all_tests