import unittest
import datetime
import os

import finetask.library.interaction as interaction
import finetask.library.exceptions as exceptions
from finetask.library.models import (
    User,
    Task,
    Planner,
    Reminder,
    Dependency,
    Permission
)


TEST_DB = 'test.db'

ROOT_NAME = 'root'
TEST_NAME = 'test'

USER_NAME = 'UserName'
USER_EMAIL = 'sample@gmail.com'

TASK_NAME_1 = 'Task1Name'
TASK_NAME_2 = 'Task2Name'
TASK_NAME_3 = 'Task3Name'
TASK_NAME_4 = 'Outdated'
TASK_CONTENT = 'TaskContent'

PLANNER_NAME = 'PlanName'
PLANNER_CONTENT = 'PlanContent'

REMINDER_TEXT = 'RemText'
REGULAR_REMINDER_TEXT = 'RemTextRegular'

GROUP_NAME = 'Group'


class TestTaskTracking(unittest.TestCase):
    """The set of 60 tests."""

    def setUp(self):
        """
        Add new data to the new database.

        Remove database file.
        Create new one by creating an Interaction object.
        Insert some objects necessary for testing.

        """

        if os.path.exists(TEST_DB):
            os.remove(TEST_DB)

        self.datetime_format = interaction.DATETIME_FORMAT
        self.controller = interaction.Interaction(TEST_DB)

        delta5mins = datetime.timedelta(minutes=5)
        now = datetime.datetime.now().strftime(self.datetime_format)
        after_now = (datetime.datetime.now() + delta5mins).strftime(self.datetime_format)
        before_now = (datetime.datetime.now() - delta5mins).strftime(self.datetime_format)

        self.user = User(USER_NAME, USER_EMAIL, rate=10)
        self.controller.add_user(self.user)

        self.task1 = Task(TASK_NAME_1, TASK_CONTENT, interaction.STATUS_CREATED, now, after_now, priority=1, owner_id=3)
        self.controller.add_task(self.task1)

        self.task2 = Task(TASK_NAME_2, TASK_CONTENT, interaction.STATUS_CREATED, now, after_now, priority=1, owner_id=3)
        self.controller.add_task(self.task2)

        self.task3 = Task(TASK_NAME_3, TASK_CONTENT, interaction.STATUS_CREATED, now, after_now, priority=1, owner_id=3)
        self.controller.add_task(self.task3)

        self.outdated_task = Task(TASK_NAME_4, TASK_CONTENT, interaction.STATUS_IN_PROGRESS, now, before_now, priority=1, owner_id=3)
        self.controller.add_task(self.outdated_task)

        self.planner = Planner(PLANNER_NAME, PLANNER_CONTENT, after_now,
                               task_priority=5, shift=5, shift_type=interaction.SHIFT_TYPE_DAYS, owner_id=3)
        self.controller.add_planner(self.planner)

        self.reminder = Reminder(REMINDER_TEXT, after_now, owner_id=3)
        self.controller.add_reminder(self.reminder)

        self.regular_reminder = Reminder(REGULAR_REMINDER_TEXT, before_now, owner_id=3, shift=5, shift_type=interaction.SHIFT_TYPE_HOURS)
        self.controller.add_reminder(self.regular_reminder)

        self.dependency = Dependency(first_id=1, second_id=2, behaviour=interaction.BEHAVIOUR_DIRECT, owner_id=3)
        self.controller.add_dependency(self.dependency)

        self.permission1 = Permission(owner_id=3, guest_id=1, task_id=1, level=interaction.PERMISSION_READ)
        self.controller.add_permission(current_user_id=3, permission=self.permission1)

        self.permission2 = Permission(owner_id=3, guest_id=2, task_id=1, level=interaction.PERMISSION_CONFIRM)
        self.controller.add_permission(current_user_id=3, permission=self.permission2)

        self.permission3 = Permission(owner_id=3, guest_id=1, task_id=2, level=interaction.PERMISSION_EDIT)
        self.controller.add_permission(current_user_id=3, permission=self.permission3)

    def test_root_user(self):
        root = self.controller.get_user_by_id(user_id=1)
        self.assertEqual(ROOT_NAME, root.nickname)

    def test_test_user(self):
        test = self.controller.get_user_by_id(user_id=2)
        self.assertEqual(TEST_NAME, test.nickname)

    def test_custom_user(self):
        current = self.controller.get_user_by_id(user_id=3)
        self.assertTrue(self.user == current)

    def test_equal_usernames(self):
        with self.assertRaises(exceptions.ExistingUserError):
            self.controller.add_user(self.user)

    def test_user_rm1(self):
        self.controller.remove_user(current_user_id=1, user_id=1)

        with self.assertRaises(exceptions.NoUserError):
            self.controller.get_user_by_id(user_id=1)

    def test_user_rm2(self):
        self.controller.remove_user(current_user_id=2, user_id=2)

        with self.assertRaises(exceptions.NoUserError):
            self.controller.get_user_by_id(user_id=2)

    def test_user_rm3(self):
        self.controller.remove_user(current_user_id=3, user_id=3)

        with self.assertRaises(exceptions.NoUserError):
            self.controller.get_user_by_id(user_id=3)

    def test_user_rm_invalid(self):
        with self.assertRaises(exceptions.NoUserError):
            self.controller.remove_user(current_user_id=5, user_id=5)

    def test_user_change_name(self):
        new_name = 'NICKNAME'
        self.controller.change_user(user_id=3, name=new_name)
        user = self.controller.get_user_by_id(user_id=3)
        self.assertEqual(user.nickname, new_name)

    def test_user_change_email(self):
        new_email = 'EMAIL'
        self.controller.change_user(user_id=3, email=new_email)
        user = self.controller.get_user_by_id(user_id=3)
        self.assertEqual(user.email, new_email)

    def test_user_change_rate(self):
        new_rate = 157
        self.controller.change_user(user_id=3, rate=new_rate)
        user = self.controller.get_user_by_id(user_id=3)
        self.assertEqual(user.rate, new_rate)

    def test_task1(self):
        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.name, TASK_NAME_1)

    def test_task2(self):
        task = self.controller.get_task_by_id(current_user_id=3, task_id=2)
        self.assertEqual(task.name, TASK_NAME_2)

    def test_task3(self):
        task = self.controller.get_task_by_id(current_user_id=3, task_id=3)
        self.assertEqual(task.name, TASK_NAME_3)

    def test_task_rm1(self):
        self.controller.remove_task(current_user_id=3, task_id=1)

        with self.assertRaises(exceptions.NoTaskError):
            self.controller.get_task_by_id(current_user_id=3, task_id=1)

    def test_task_rm2(self):
        self.controller.remove_task(current_user_id=3, task_id=2)

        with self.assertRaises(exceptions.NoTaskError):
            self.controller.get_task_by_id(current_user_id=3, task_id=2)

    def test_task_rm3(self):
        self.controller.remove_task(current_user_id=3, task_id=3)

        with self.assertRaises(exceptions.NoTaskError):
            self.controller.get_task_by_id(current_user_id=3, task_id=3)

    def test_task_rm_invalid(self):
        with self.assertRaises(exceptions.NoTaskError):
            self.controller.remove_task(current_user_id=3, task_id=158)

    def test_task_change_name(self):
        new_name = 'NEWNAME'
        self.controller.change_task(current_user_id=3, task_id=1, name=new_name)
        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.name, new_name)

    def test_task_change_content(self):
        new_content = 'NEWCONTENT'
        self.controller.change_task(current_user_id=3, task_id=1, content=new_content)
        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.content, new_content)

    def test_task_change_priority(self):
        new_priority = 1001
        self.controller.change_task(current_user_id=3, task_id=1, priority=new_priority)
        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.priority, new_priority)

    def test_task_change_status(self):
        self.controller.change_task(current_user_id=3, task_id=3, status=interaction.STATUS_FAILED)
        task = self.controller.get_task_by_id(current_user_id=3, task_id=3)
        self.assertEqual(task.status, interaction.STATUS_FAILED)

    def test_task_change_status_invalid_by_dependency(self):
        with self.assertRaises(exceptions.StatusNotAllowedError):
            self.controller.change_task(current_user_id=3, task_id=1, status=interaction.STATUS_DONE)

        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.status, interaction.STATUS_CREATED)

    def test_task_change_status_valid_by_dependency(self):
        with self.assertRaises(exceptions.StatusNotAllowedError):
            self.controller.change_task(current_user_id=3, task_id=1, status=interaction.STATUS_DONE)

        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.status, interaction.STATUS_CREATED)

        self.controller.change_task(current_user_id=3, task_id=2, status=interaction.STATUS_DONE)
        self.controller.change_task(current_user_id=3, task_id=1, status=interaction.STATUS_DONE)
        task = self.controller.get_task_by_id(current_user_id=3, task_id=1)
        self.assertEqual(task.status, interaction.STATUS_DONE)

    def test_planner(self):
        planner = self.controller.get_planner_by_id(current_user_id=3, planner_id=1)
        self.assertEqual(planner.task_name, PLANNER_NAME)

    def test_planner_rm(self):
        self.controller.remove_planner(current_user_id=3, planner_id=1)

        with self.assertRaises(exceptions.NoPlannerError):
            self.controller.get_planner_by_id(current_user_id=3, planner_id=1)

    def test_planner_rm_invalid(self):
        with self.assertRaises(exceptions.NoPlannerError):
            self.controller.remove_planner(current_user_id=3, planner_id=133)

    def test_planner_make_task(self):
        task = self.controller.get_task_by_id(current_user_id=3, task_id=5)
        self.assertEqual(task.name, PLANNER_NAME)

    def test_planner_change_name(self):
        new_name = 'NEWPLANNERNAME'
        self.controller.change_planner(current_user_id=3, planner_id=1, name=new_name)
        planner = self.controller.get_planner_by_id(current_user_id=3, planner_id=1)
        self.assertEqual(planner.task_name, new_name)

    def test_planner_change_content(self):
        new_content = 'NEWPLANNERCONTENT'
        self.controller.change_planner(current_user_id=3, planner_id=1, content=new_content)
        planner = self.controller.get_planner_by_id(current_user_id=3, planner_id=1)
        self.assertEqual(planner.task_content, new_content)

    def test_reminder1(self):
        reminder = self.controller.get_reminder_by_id(current_user_id=3, reminder_id=1)
        self.assertEqual(reminder.text, REMINDER_TEXT)

    def test_reminder2(self):
        reminder = self.controller.get_reminder_by_id(current_user_id=3, reminder_id=2)
        self.assertEqual(reminder.text, REGULAR_REMINDER_TEXT)

    def test_reminder_rm1(self):
        self.controller.remove_reminder(current_user_id=3, reminder_id=1)

        with self.assertRaises(exceptions.NoReminderError):
            self.controller.get_reminder_by_id(current_user_id=3, reminder_id=1)

    def test_reminder_rm2(self):
        self.controller.remove_reminder(current_user_id=3, reminder_id=2)

        with self.assertRaises(exceptions.NoReminderError):
            self.controller.get_reminder_by_id(current_user_id=3, reminder_id=2)

    def test_reminder_rm_invalid(self):
        with self.assertRaises(exceptions.NoReminderError):
            self.controller.remove_reminder(current_user_id=3, reminder_id=1589)

    def test_reminder_change_text(self):
        new_text = 'NEWREMTEXT'
        self.controller.change_reminder(current_user_id=3, reminder_id=1, text=new_text)
        reminder = self.controller.get_reminder_by_id(current_user_id=3, reminder_id=1)
        self.assertEqual(reminder.text, new_text)

    def test_reminder_change_time(self):
        new_time = '23.10.2020 13:20'
        self.controller.change_reminder(current_user_id=3, reminder_id=2, time=new_time)
        reminder = self.controller.get_reminder_by_id(current_user_id=3, reminder_id=2)
        self.assertEqual(reminder.invoke_time, new_time)

    def test_dependency(self):
        dep = self.controller.get_dependency_by_task_id(current_user_id=3, task_id=1)
        self.assertEqual(dep.behaviour, interaction.BEHAVIOUR_DIRECT)

    def test_dependency_rm(self):
        self.controller.remove_dependency(current_user_id=3, task_id=1)

        with self.assertRaises(exceptions.NoDependencyError):
            self.controller.get_dependency_by_task_id(current_user_id=3, task_id=1)

    def test_dependency_rm_invalid(self):
        with self.assertRaises(exceptions.NoDependencyError):
            self.controller.remove_dependency(current_user_id=3, task_id=133)

    def test_dependency_add_where_already_exists(self):
        dep = Dependency(first_id=1, second_id=2, behaviour=interaction.BEHAVIOUR_INVERSE, owner_id=3)

        with self.assertRaises(exceptions.ExistingDependencyError):
            self.controller.add_dependency(dep)

    def test_deadlines_failing(self):
        self.controller.fail_all_by_deadline(user_id=3)
        failed = self.controller.get_task_by_id(current_user_id=3, task_id=4)
        self.assertEqual(failed.status, interaction.STATUS_FAILED)

    def test_groups_standard(self):
        groups = self.controller.get_all_groups(owner_id=3)
        self.assertEqual(groups, [interaction.GROUP_ALL, interaction.GROUP_CURRENT, interaction.GROUP_ENDED,
                                  interaction.GROUP_DONE, interaction.GROUP_FAILED, interaction.GROUP_ARCHIEVE])

    def test_groups_custom(self):
        self.controller.move_task_to_group(task_id=1, group_name=GROUP_NAME)
        groups = self.controller.get_all_groups(owner_id=3)
        self.assertEqual(groups, [interaction.GROUP_ALL, interaction.GROUP_CURRENT, interaction.GROUP_ENDED,
                                  interaction.GROUP_DONE, interaction.GROUP_FAILED, interaction.GROUP_ARCHIEVE, GROUP_NAME])

    def test_tasks_show_in_group1(self):
        self.controller.move_task_to_group(task_id=1, group_name=GROUP_NAME)
        tasks = self.controller.get_tasks_by_group_name(user_id=3, group_name=GROUP_NAME)
        self.assertEqual(tasks[0].name, TASK_NAME_1)

    def test_tasks_show_in_group2(self):
        self.controller.move_task_to_group(task_id=4, group_name=GROUP_NAME)
        tasks = self.controller.get_tasks_by_group_name(user_id=3, group_name=GROUP_NAME)
        self.assertEqual(tasks[0].name, TASK_NAME_4)

    def move_4_tasks_to_group(self):
        self.controller.move_task_to_group(task_id=1, group_name=GROUP_NAME)
        self.controller.move_task_to_group(task_id=2, group_name=GROUP_NAME)
        self.controller.move_task_to_group(task_id=3, group_name=GROUP_NAME)
        self.controller.move_task_to_group(task_id=4, group_name=GROUP_NAME)

    def test_tasks_show_in_group_3(self):
        self.move_4_tasks_to_group()
        tasks = self.controller.get_tasks_by_group_name(user_id=3, group_name=GROUP_NAME)

        self.assertEqual(tasks[0].name, TASK_NAME_1)
        self.assertEqual(tasks[1].name, TASK_NAME_2)
        self.assertEqual(tasks[2].name, TASK_NAME_3)
        self.assertEqual(tasks[3].name, TASK_NAME_4)

    def remove_first_2_tasks_from_group(self):
        self.controller.remove_task_from_group(task_id=1, group_name=GROUP_NAME)
        self.controller.remove_task_from_group(task_id=2, group_name=GROUP_NAME)

    def remove_last_2_tasks_from_group(self):
        self.controller.remove_task_from_group(task_id=3, group_name=GROUP_NAME)
        self.controller.remove_task_from_group(task_id=4, group_name=GROUP_NAME)

    def test_rm_from_group1(self):
        self.move_4_tasks_to_group()
        self.remove_first_2_tasks_from_group()
        tasks = self.controller.get_tasks_by_group_name(user_id=3, group_name=GROUP_NAME)

        self.assertEqual(tasks[0].name, TASK_NAME_3)
        self.assertEqual(tasks[1].name, TASK_NAME_4)

    def test_rm_from_group2(self):
        self.move_4_tasks_to_group()
        self.remove_last_2_tasks_from_group()
        tasks = self.controller.get_tasks_by_group_name(user_id=3, group_name=GROUP_NAME)

        self.assertEqual(tasks[0].name, TASK_NAME_1)
        self.assertEqual(tasks[1].name, TASK_NAME_2)

    def test_access_read_true(self):
        can_read = self.controller.has_read_access(user_id=1, task_id=1)
        self.assertTrue(can_read)

    def test_access_read_false(self):
        can_read = self.controller.has_read_access(user_id=1, task_id=3)
        self.assertFalse(can_read)

    def test_access_confirm_true(self):
        can_confirm = self.controller.has_confirm_access(user_id=2, task_id=1)
        self.assertTrue(can_confirm)

    def test_access_confirm_false(self):
        can_confirm = self.controller.has_confirm_access(user_id=2, task_id=3)
        self.assertFalse(can_confirm)

    def test_access_edit_true(self):
        can_edit = self.controller.has_edit_access(user_id=1, task_id=2)
        self.assertTrue(can_edit)

    def test_access_edit_false(self):
        can_edit = self.controller.has_edit_access(user_id=2, task_id=2)
        self.assertFalse(can_edit)

    def test_access_delete_true(self):
        can_delete = self.controller.has_delete_access(user_id=3, task_id=1)
        self.assertTrue(can_delete)

    def test_access_delete_false(self):
        can_delete = self.controller.has_delete_access(user_id=1, task_id=1)
        self.assertFalse(can_delete)

    def test_owner(self):
        can_read = self.controller.has_read_access(user_id=3, task_id=1)
        can_confirm = self.controller.has_confirm_access(user_id=3, task_id=2)
        can_edit = self.controller.has_edit_access(user_id=3, task_id=3)
        can_delete = self.controller.has_delete_access(user_id=3, task_id=4)

        self.assertTrue(can_read)
        self.assertTrue(can_confirm)
        self.assertTrue(can_edit)
        self.assertTrue(can_delete)

    def test_no_permissions_1(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.get_task_by_id(current_user_id=1, task_id=4)

    def test_no_permissions_2(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.get_planner_by_id(current_user_id=1, planner_id=1)

    def test_no_permissions_3(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.get_reminder_by_id(current_user_id=1, reminder_id=1)

    def test_no_permissions_4(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.change_task(current_user_id=1, task_id=4, status=interaction.STATUS_DONE)

    def test_no_permissions_5(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.change_task(current_user_id=1, task_id=4, priority=100)

    def test_no_permissions_6(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.change_planner(current_user_id=1, planner_id=1, name='NAME')

    def test_no_permissions_7(self):
        with self.assertRaises(exceptions.PermissionDeniedError):
            self.controller.change_reminder(current_user_id=1, reminder_id=1, text='TEXT')

    def tearDown(self):
        os.remove(TEST_DB)


if __name__ == '__main__':
    unittest.main()
