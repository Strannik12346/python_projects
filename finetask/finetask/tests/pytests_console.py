import subprocess
import pytest
import json
import os

from finetask.library.interaction import TEST_USERNAME
from finetask.tests.get_tests_console import get_test_data


APPLICATION_NAME = 'finetask'
TEST_DIRECTORY = 'tests'
TEST_DATABASE = 'finetask_test_database.db'
LOGGING_FILE = 'finetask.log'

USER_JSON = 'user.json'
LOG_LEVEL = 'info'


class TestExternal:
    def setup_class(self):
        current_dir = os.getcwd()
        test_directory = os.path.join(current_dir, TEST_DIRECTORY)

        self.user_file = os.path.join(test_directory, USER_JSON)
        self.log_file = os.path.join(test_directory, LOGGING_FILE)
        self.test_database = os.path.join(test_directory, TEST_DATABASE)  # To delete correctly

        if os.path.exists(self.test_database):
            os.remove(self.test_database)

        user_data = json.dumps(
            {
                'database': self.test_database,
                'log': self.log_file,
                'logging': LOG_LEVEL,
                'name': TEST_USERNAME
            },
            sort_keys=True, indent=4
        )

        user_data += '\n'

        if not os.path.exists(test_directory):
            os.mkdir(test_directory)

        with open(self.user_file, 'w') as file:
            file.write(user_data)
        print(user_data)

    def print_finetask_output(self, finetask_args_list):
        config_argument = '--config=' + self.user_file
        args = [APPLICATION_NAME, config_argument] + finetask_args_list
        print(subprocess.check_output(args, universal_newlines=True))

    def return_finetask_output(self, finetask_args_list):
        config_argument = '--config=' + self.user_file
        args = [APPLICATION_NAME, config_argument] + finetask_args_list
        return subprocess.check_output(args, universal_newlines=True)

    def count_lines(self, str_output):
        output_lines = str_output.splitlines()

        result = len(output_lines)
        for el in output_lines:
            if len(el) < 3:
                result -= 1

        return result

    def has_all_substrings(self, text, substrings):
        text = text.upper()
        result = True

        for substring in substrings:
            substring = substring.upper()
            result = result and substring in text
        return result

    @pytest.mark.parametrize("test_input, expected_lines_count, expected_substrings", get_test_data())
    def test_eval(self, test_input, expected_lines_count, expected_substrings):
        """Parametrized tests. Tests are generated with array of tuples by generate_tests()."""

        response = self.return_finetask_output(test_input)
        lines_count = self.count_lines(response)

        assert lines_count == expected_lines_count
        assert self.has_all_substrings(response, expected_substrings)

    def teardown_class(self):
        if os.path.exists(self.user_file):
            os.remove(self.user_file)
        if os.path.exists(self.test_database):
            os.remove(self.test_database)


if __name__ == '__main__':
    pytest.main(['pytests_console.py', '-s'])
